
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "bajaIMS.h"
#include "XBee.h"
#include "lcd.h"
#include "IMU.h"
#include "sd_card.h"
#include "gps.h"

#define DMA_CNDTR3 *(uint32_t volatile *) 0x40020034
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
static uint8_t displayPitMessage = 0;
/**
We will be using DMA to store values obtained by the various 
ADC channels. The related values are given below

0 -> CH4 -> Battery

1 -> CH5 -> firstFuel
2 -> CH9 -> secondFuel
3 -> CH10-> thirdFuel

4 -> CH6 -> engineTemp
5 -> CH8 -> cvtTemp
*/
static uint32_t ADC_BUFFER[6] = {0};
static uint32_t rpmTimer[1] = {0};

static struct PitMessage pit;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/**
* @brief This function handles USART1 global interrupt.
*/
void USART1_IRQHandler(void)
{
  static uint8_t arrayIndex = 0;
  static uint8_t end = 0;
	
  pit.message[arrayIndex] = huart1.Instance->DR;
  if (arrayIndex > MAX_PIT_TRANSMIT)
    arrayIndex = 0;
  if (end == 0)
  {
    if (pit.message[arrayIndex] == 'E')
      end++;
  }
  else if (end == 1)
  {
    if (pit.message[arrayIndex] == 'N')
      end++;
  }
  else if (end == 2)
  {
    if (pit.message[arrayIndex] == 'D')
    {
			pit.size = arrayIndex;
      end = 0;
      arrayIndex = 0;
			displayPitMessage = 1;
    }
  }
  HAL_UART_IRQHandler(&huart1);
}

unsigned rpmCalc (void)
{
  unsigned i, sum = 0;
	
  for (i = 0; i < 10; i++)
    sum += rpmTimer[0];
  sum /= 10;

  sum = (100000/sum) + 1;
  return sum;
	
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t receiveChar;
	
  volatile uint32_t adcStore[4];
	uint32_t rpmValue;
	uint8_t brake = BRAKE_RELEASED;
	uint8_t gear = NEUTRAL;
	uint32_t fuelLevel = 100;
	
  struct gpsParsed dataOut;
	uint8_t outBuf[MSG_LEN] = {0};
  uint8_t parseInput[MSG_LEN] = {0};
	axis accel;
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_SPI3_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_IC_Start_DMA (&htim2, TIM_CHANNEL_4, (uint32_t*)rpmTimer, 1);
  HAL_ADC_Start_DMA(&hadc1, ADC_BUFFER,6);
  HAL_UART_Receive_IT(&huart1, &receiveChar, 1);
  HAL_UART_Receive_DMA(&huart2, outBuf, MSG_LEN);
	
  lcdInit();
  specCharInit();
  
	HAL_UART_Transmit(&huart2, (uint8_t*)initStrProtocols, 51, 1000);
	HAL_UART_Transmit(&huart2, (uint8_t*)initStrRate, 17, 1000);
	
//  initLSM6DS3 (orientation_Pin);
  sd_init ();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) 
  {
    if (huart1.Instance->CR1 != 0x212C)
      huart1.Instance->CR1 = 0x212C;

		// Below is code for collecting data from the various sensors
		
		// grabs data from the adc dma buffer
//    adcStore[0] = ADC_BUFFER[0]; // Batery Level
//    adcStore[1] = ADC_BUFFER[1] + ADC_BUFFER[2] + ADC_BUFFER[3]; // Fuel Level
//    adcStore[2] = ADC_BUFFER[4]; // Engine Temp
//    adcStore[3] = ADC_BUFFER[5]; // CVT Temp
    rpmValue = rpmCalc (); // Engine RPM
		
		// Gear State
		if (HAL_GPIO_ReadPin(GPIOD, NEUTRAL_Pin))
			gear = NEUTRAL;
		if (HAL_GPIO_ReadPin(GPIOD, REVERSE_Pin))
			gear = REVERSE;
		if ( !HAL_GPIO_ReadPin(GPIOD, NEUTRAL_Pin) && !HAL_GPIO_ReadPin(GPIOD, REVERSE_Pin))
			gear = DRIVE;
		// Brake State
		if (HAL_GPIO_ReadPin(GPIOD, BRAKE_Pin))
			brake = BRAKE_ENGAGED;
		else 
			brake = BRAKE_RELEASED;
		
    // collect the gps data
		if (DMA_CNDTR3 == 0x0)
		{
			//Stop the receieve process so that the buffer is not overwritten
			HAL_UART_DMAStop(&huart2);
			//Copy the array
			copyArrayToStar(outBuf, parseInput, MSG_LEN);
			
			if (starCount(parseInput, MSG_LEN) == 2)
			{
				//Parse the input into the approriate locations
				parseGPSStr(parseInput, &dataOut);
				
				//Structure the data such that it can be transmitted
			}
      clearArraySized(outBuf, MSG_LEN);			
			HAL_UART_Receive_DMA(&huart2, outBuf, MSG_LEN);
		}
		
		
		printInfoArea (&dataOut, rpmValue, brake, gear, fuelLevel,
		               displayPitMessage, pit.message, pit.size, adcStore[2]);
    
//    accel = readAccel (orientation_Pin);
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /**Configure the Systick interrupt time 
    */
  __HAL_RCC_PLLI2S_ENABLE();

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
