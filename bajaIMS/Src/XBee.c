#include "XBee.h"

unsigned charArrayCompareXbee (uint8_t* str1, const uint8_t* str2, unsigned size)
{
	unsigned i = 0;
	unsigned end = 0, offset = 0;
	if (str1[0] == 0x00)
		return 0;
	while (str1[i++] != str2[0]) {offset++;}
	
	for (i = 0; (i < size); i++)
	{
		if (str1[i] != str2[i])
		{
			return 0;
		}
		if (end == 0)
		{
			if (str1[i] == CHECK_E || str2[i] == CHECK_E)
				end++;
		}
		else if (end == 1)
		{
			if (str1[i] == CHECK_N || str2[i] == CHECK_N)
				end++;
		}
		else if (end == 2)
		{
			if (str1[i] == CHECK_D || str2[i] == CHECK_D)
			{
				return 1;
			}
		}
		else
		{
			end = 0;
		}
	}
	
	return 0;
}





