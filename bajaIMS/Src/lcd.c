#include "lcd.h"
#include "gpio.h"

void lcdDataLoad(uint8_t data)
{
  // reset all the LCD data pins (D0->D7)
  HAL_GPIO_WritePin(GPIOE, LCD_D0_Pin|LCD_D1_Pin|LCD_D2_Pin|LCD_D3_Pin
                           |LCD_D4_Pin|LCD_D5_Pin|LCD_D6_Pin|LCD_D7_Pin, 
                           GPIO_PIN_RESET);
  // set relevant pins to the data value
  GPIOE->ODR |= (data << 8);
}

void lcdInit(void)
{	
  // Incase there has not been enough time since startup for
	// LCD to be ready to receive commands, delay 5 microseconds
  HAL_Delay(5);
  //LCD Configure routine
  commandToLCD(LCD_8B2L);
  commandToLCD(LCD_8B2L);
  commandToLCD(LCD_8B2L);
  commandToLCD(LCD_8B2L);
  commandToLCD(LCD_DCB);
  commandToLCD(LCD_CLR);
  commandToLCD(LCD_MCR);

  // Print out the system name
  HAL_Delay(5);
  curserLCD (0,3);
  printString((uint8_t*)MAD,15);
  curserLCD(1,2);
  printString((uint8_t*)BAJA, 17);
  curserLCD (2,5);
  printString((uint8_t*)MONITORING, 10);
  curserLCD (3,7);
  printString((uint8_t*)SYSTEM, 6);
}

void commandToLCD(uint8_t data)
{
  // toggle pins to set to command mode
  LCD_RS_LOW;
  LCD_ENA_HIGH;
  // send data value
  lcdDataLoad(data);
  // wait then set back to character mode
  HAL_Delay(1);
  LCD_RS_LOW;
  LCD_ENA_LOW;
}

void dataToLCD(uint8_t character)
{
  LCD_RS_HIGH;
  LCD_ENA_HIGH;
  LCD_RW_LOW;
  lcdDataLoad(character);
  HAL_Delay (1); 
  LCD_RS_HIGH;
  LCD_ENA_LOW;
  LCD_RW_LOW;
}

void clearRowToColLCD(uint8_t row, uint8_t col1, uint8_t col2)
{
  uint8_t addr = 0x00;
  uint8_t colNum = 0x00;
  uint8_t i = 0;
	
  if (row == 0) //First row
  {
    //Takes move cursor command 0x80 adds it to first line address 0x00 and adds the selected col number
    addr = 0x80 + 0x00 + col1;
  }
  else if (row == 1) //Second row
  {
  //Takes move cursor command 0x80 adds it to second line address 0x40 and adds the selected col number
    addr = 0x80 + 0x40 + col1;
  }
  else if (row == 2) //Third row
  {
    //Takes move cursor command 0x80 adds it to second line address 0x14 and adds the selected col number
    addr = 0x80 + 0x14 + col1;
  }
  else if (row == 3) //Fourth row
  {
    //Takes move cursor command 0x80 adds it to second line address 0x54 and adds the selected col number
    addr = 0x80 + 0x54 + col1;
  }
	
  //Finds location
  commandToLCD(addr);
	
  //Determines how many column spaces to clear
  colNum = col2 - col1;
	
  //Clears data with space character within designated character spaces
  while(i != colNum)
  {
    dataToLCD(0x20);
    i++;
  }
}

void curserLCD(uint8_t row, uint8_t col)
{
  uint8_t addr = 0x00;
	
  if (row == 0) //First row
  {
    //Takes move cursor command 0x80 adds it to first line address 0x00 and adds the selected col number
    addr = 0x80 + 0x00 + col;
  }
  else if (row == 1) //Second row
  {
    //Takes move cursor command 0x80 adds it to second line address 0x40 and adds the selected col number
    addr = 0x80 + 0x40 + col;
  }
  else if (row == 2) //Third row
  {
    //Takes move cursor command 0x80 adds it to second line address 0x14 and adds the selected col number
    addr = 0x80 + 0x14 + col;
  }
  else if (row == 3) //Fourth row
  {
    //Takes move cursor command 0x80 adds it to second line address 0x54 and adds the selected col number
    addr = 0x80 + 0x54 + col;
  }
	
  //Sends command to move cursor to designated location on display
  commandToLCD(addr);
}

void clearLCD(void)
{
  commandToLCD(LCD_CLR);
}

void printSpeed(uint8_t speed)
{
  uint8_t d1, d0;
	
  //Digit one
  d1 = (speed / 10) % 10;
	
  //Digit two
  d0 = speed % 10;
	
  //Determines digit one and prints it
  switch(d1)
  {
    case 0: printZero(1,0); break;
    case 1: printOne(1,0); break;
    case 2: printTwo(1,0); break;
    case 3: printThree(1,0); break;
    case 4: printFour(1,0); break;
    case 5: printFive(1,0); break;
    case 6: printSix(1,0); break;
    case 7: printSeven(1,0); break;
    case 8: printEight(1,0); break;
    case 9: printNine(1,0); break;
    default : printZero(1,0); break;
  }
	
  //Determines digit two and prints it
  switch(d0)
  {
    case 0: printZero(1,3); break;
    case 1: printOne(1,3); break;
    case 2: printTwo(1,3); break;
    case 3: printThree(1,3); break;
    case 4: printFour(1,3); break;
    case 5: printFive(1,3); break;
    case 6: printSix(1,3); break;
    case 7: printSeven(1,3); break;
    case 8: printEight(1,3); break;
    case 9: printNine(1,3); break;
    default : printZero(1,3); break;
  }
}


void specCharInit(void)
{
  uint8_t i = 0;
	
  //Generate an array to make 8 custom characters
  const unsigned short MyChar5x8[] =
  {
    0x00, 0x00, 0x00, 0x00, 0x03, 0x07, 0x0F, 0x1F, // Code for char num #0 (Top left corner)
    0x00, 0x00, 0x00, 0x00, 0x18, 0x1C, 0x1E, 0x1F, // Code for char num #1	(Top right corner)
    0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x1F, 0x1F, // Code for char num #2 (Bottom half)
    0x00, 0x00, 0x00, 0x00, 0x1F, 0x0F, 0x07, 0x03,	// Code for char num #3 (Bottom left corner)
    0x00, 0x00, 0x00, 0x00, 0x1F, 0x1E, 0x1C, 0x18, // Code for char num #4 (Bottom right corner)
    0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x0F, 0x07, 0x03,	// Code for char num #5 (Long bottom left corner)
    0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1E, 0x1C, 0x18, // Code for char num #6 (Long bottom right corner)
    0x1C, 0x18, 0x1C, 0x18, 0x1C, 0x18, 0x1C, 0x18 // Code for char num #7 (Half)
  };
	
  //First address of custom character memory address of LCD
  commandToLCD(0x40);
	
  //Goes through each byte and uploading them into the LCD
  while(i++ <= 63)
    dataToLCD(MyChar5x8[i]);

  //Return home to get out of custom character generation
  commandToLCD(0x02);
}

void printBlank(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0x20);
	
  curserLCD(row,col + 1);
  dataToLCD(0x20);
	
  curserLCD(row,col + 2);
  dataToLCD(0x20);
	
  curserLCD(row + 1,col);
  dataToLCD(0x20);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(0x20);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(0x20);
}

void printZero(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
	dataToLCD(0);
	
	curserLCD(row,col + 1);
	dataToLCD(2);
	
	curserLCD(row,col + 2);
	dataToLCD(1);
	
	curserLCD(row + 1,col);
	dataToLCD(0xFF);
	
	curserLCD(row + 1,col + 1);
	dataToLCD(0x20);
	
	curserLCD(row + 1,col + 2);
	dataToLCD(0xFF);
	
	curserLCD(row + 2,col);
	dataToLCD(5);
	
	curserLCD(row + 2,col + 1);
	dataToLCD(2);
	
	curserLCD(row + 2,col + 2);
	dataToLCD(6);
}

void printOne(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(0x20);

  curserLCD(row + 1,col);
  dataToLCD(0x20);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(0xff);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(0xff);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(2);
}

void printTwo(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 1,col);
  dataToLCD(0);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(6);
	
  curserLCD(row + 2,col);
  dataToLCD(5);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(4);
}

void printThree(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 1,col);
  dataToLCD(0x20);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(0xff);
	
  curserLCD(row + 2,col);
  dataToLCD(3);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(6);
}

void printFour(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(2);
	
  curserLCD(row,col + 1);
  dataToLCD(0x20);
	
  curserLCD(row,col + 2);
  dataToLCD(2);
	
  curserLCD(row + 1,col);
  dataToLCD(5);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(0xff);
	
  curserLCD(row + 2,col);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(0xff);
}

void printFive(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(2);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(4);
	
  curserLCD(row + 1,col);
  dataToLCD(0xff);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 2,col);
  dataToLCD(3);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(6);
}

void printSix(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 1,col);
  dataToLCD(0xff);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 2,col);
  dataToLCD(5);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(6);
}

void printSeven(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(3);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(2);
	
  curserLCD(row + 1,col);
  dataToLCD(0x20);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(0);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(6);
	
  curserLCD(row + 2,col);
  dataToLCD(0x20);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(0xff);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(0x20);
}

void printEight(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 1,col);
  dataToLCD(0xff);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(0xff);
	
  curserLCD(row + 2,col);
  dataToLCD(5);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(6);
}

void printNine(uint8_t row, uint8_t col)
{
  curserLCD(row,col);
  dataToLCD(0);
	
  curserLCD(row,col + 1);
  dataToLCD(2);
	
  curserLCD(row,col + 2);
  dataToLCD(1);
	
  curserLCD(row + 1,col);
  dataToLCD(5);
	
  curserLCD(row + 1,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 1,col + 2);
  dataToLCD(0xff);
	
  curserLCD(row + 2,col);
  dataToLCD(3);
	
  curserLCD(row + 2,col + 1);
  dataToLCD(2);
	
  curserLCD(row + 2,col + 2);
  dataToLCD(6);
}

void fuelSetup(void)
{
  uint8_t i = 0;
  // make sure that the top row of LCD is cleared
	clearRowToColLCD(0,1,19);
  
  // set cursor to the top left corner (0,0) and print E (Empty)
  curserLCD(0,0);
  dataToLCD('E');
	
	//Prints full fuel tank
  while(i++ <= 18)
    dataToLCD(0xff); //print blocks till full tank displayed

  // print out F (Full)
  dataToLCD('F');
}

void printFuelLevel(uint8_t value)
{
  static uint8_t current_fuel = FULL_FUEL;

  uint8_t i = 0;
  uint8_t convert = (value * 18)/100;
	
	//Safe guards to make sure the fuel level stays below 18
  if (convert > 18)
    convert = 18;

  // if convert is a new value, print the gauge again
  if (convert != current_fuel)
  {
    curserLCD(0,1);
    while (i <= convert)
      dataToLCD(0xff);
    while (i <= FULL_FUEL)
      dataToLCD(0x20);
  }
	
	//Update the current fuel level
  current_fuel = convert;
	
	//If the fuel tank is less than or equal to 3 ticks the fuel tank is low
  if (convert <= 3)
  {
    //Print low fuel
    curserLCD(0,6);
    printString((uint8_t*)LOW_FUEL, 8);
  }
}

void clearInfoArea(void)
{
  clearRowToColLCD(1,7,20);
  clearRowToColLCD(2,7,20);
  clearRowToColLCD(3,7,20);
}

void printAlert(uint8_t alertCode)
{
  curserLCD(1,7);
  printString((uint8_t*)ALERT, 13);
	
	switch(alertCode)
	{
    case 1:
      curserLCD(2,7);
      printString((uint8_t*)RETURN_PIT, 13);
      break;
    case 2:
      curserLCD(2,10);
      printString((uint8_t*)SEND_IT, 7);
      break;
    case 3:
      curserLCD(2,7);
      printString((uint8_t*)ONE_MORE, 12);
      break;
    default:
      curserLCD(2,10);
      printString((uint8_t*)GG, 7);
      break;
  }
	
  curserLCD(3,7);
  printString((uint8_t*)STARS, 13);
}


uint8_t speedCalc (uint8_t* speed, uint8_t size)
{
  unsigned i, val;
	
  for (i = 0; i < size; i ++)
  {
    if(speed[i] == '.')
      return val;
    if (!checkNumeric(speed[i]))
      return 0;
    else
      val = (val * 10) + (speed[i] - 48);
  }
  return val;
}

void printUint (uint16_t input)
{
  int i;
  int digit;
  int print = 0;
  uint8_t ascii;
	
  if ((input & 0x8000) == 0x8000)
  {
    input &= ~(0x8000);
    dataToLCD('-');
  }
  // the constant is the maximum # of digits a int16 can have
  for (i = MAGNITUDE_OF_UINT16; i >= 0; i--)
  {
    // We single out the MSB in question
    digit = (input / power (10, i));
    input %= power (10, i);
    // Then check to see if it should be printed
    if ((print != 0) || (digit != 0))
    {
      print = 1;
      ascii = digit + NUMERIC_LOWER_LIMIT;
      dataToLCD(ascii);
    }
  }
}

int power (int base, int exp)
{
  int result = 1;
  int i;
	
  for (i = 0; i < exp; i++)
  {
    result *= base;
  }
	
  return result;
}

uint8_t checkNumeric(uint8_t input)
{
  if ((input >= NUMERIC_LOWER_LIMIT) && (input <= NUMERIC_UPPER_LIMIT))
    return 1;
  return 0;
}

uint8_t checkAlphaCap(uint8_t input)
{
  if ((input >= ALPHA_LOWER_LIMIT) && (input <= ALPHA_MIDDLE_LIMIT))
    return 1;
  return 0;
}

uint8_t checkAlphaLow(uint8_t input)
{
  if ((input >= ALPHA_MIDDLE_LIMIT) && (input <= ALPHA_UPPER_LIMIT))
    return 1;
  return 0;
}

uint8_t checkAscii (uint8_t input)
{
  if ((input >= VALID_ASCII_LOWER) && (input <= VALID_ASCII_UPPER))
    return 1;
  return 0;
}

void printString (uint8_t* str, unsigned size)
{
  unsigned i;

  for (i = 0; i < size; i++)
  {
    if (checkAscii(str[i]))
      dataToLCD(str[i]);
  }
}

void printInfoArea (struct gpsParsed *velocity, uint32_t rpm, uint8_t brake,
                    uint8_t gear, uint32_t fuelLevel, uint8_t messageDisplay,
                    uint8_t *message, uint8_t messageLen, uint32_t eTemp)
{
	uint8_t i, lineNum, oldLineNum, totalLines, newLine;
	clearInfoArea ();
	
	if (!messageDisplay)
	{
		curserLCD(2,14);
		dataToLCD ('T');
		dataToLCD (':');
		printUint(eTemp);
		
		curserLCD (3, 14);
		dataToLCD('D');
		dataToLCD (':');
		printString (velocity->direction, velocity->direction_size);
		
		printSpeed (speedCalc(velocity->velocity, velocity->velocity_size));
		
		curserLCD(3,7);
		dataToLCD('G');
		dataToLCD (':');
		dataToLCD(gear);
		
		if (brake)
			dataToLCD(0xff);
		
		curserLCD(2,7);
		dataToLCD('R');
		dataToLCD(':');
		rpm = rpm *60;
		printUint(rpm);
		
		curserLCD(0, 0);
		dataToLCD('E');
		curserLCD(0, 19);
		dataToLCD('F');
		printFuelLevel (fuelLevel);
	}
	else
	{
		newLine = 0;
		lineNum = 0;
		oldLineNum = 0;
		// divide by 13 to get the number of lines to print on
		// subtract 3 to disinclude the footer "END"
		totalLines = (messageLen-3) / 13;
		curserLCD (3, 7);
		
		for (i = 0; i < (messageLen-3); i++)
		{
			if (newLine)
			{
				newLine = 0;
				curserLCD (messageLen/i, 7);
			}
			lineNum = i/13;
			if (lineNum != oldLineNum)
			{
				oldLineNum = lineNum;
				newLine = 1;
			}
			dataToLCD(message[i]);
		}
	}
}

