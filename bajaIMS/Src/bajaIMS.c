#include "bajaIMS.h"



unsigned charArrayCompare (uint8_t* str1, const uint8_t* str2, unsigned size)
{
	int i;
	
	for (i = 0; (i < size); i++)
	{
		if (str1[i] != str2[i])
			return 0;
	}
	
	return 1;
}

unsigned charArrayCount(uint8_t* str)
{
	unsigned end = 0;
	unsigned i = 0;
	while (!end)
	{
		if (str[i] == 0x00)
			end = 1;
		else 
			i++;
	}
	return i;
}


void clearArraySized (uint8_t* str, uint32_t arrSize)
{
	unsigned i;
	for (i = 0; i < arrSize; i++)
		str[i] = (char)0;
}

void copyArray (uint8_t* str1, uint8_t* str2, unsigned byteLen)
{
	unsigned i;
	for (i = 0; i < byteLen; i++)
		str2[i] = str1[i];
}
