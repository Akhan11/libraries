#include "gps.h"
#include "bajaIMS.h"

void copyArrayToStar (uint8_t* str1, volatile uint8_t* str2, uint32_t len)
{
	unsigned i = 0, g = 0, h = 0, j = 0;
	
	//Find the start of the first sentence
	while (str1[h] != '$')
	{
		h++;
		if (h >= len)
		{
			//exit if we reach 140 characters and still havent found the $
			return;
		}
	}
	
	for (g = 0; g < 2; g++)
	{
		while (str1[i+h] != '*')
		{
			str2[i] = str1[i+h];
			i++;
			
			if (i == len)
			{
				//exit if we reach 140 characters and still havent found the *
				i = 0;
				if (j > 0)
					return;
				j++;
			}
		}
		str2[i] = '*';
		i++;
	}
}

// parse through a string and return the number of *'s seen
uint8_t starCount(uint8_t* gpsStr, uint32_t len)
{
	uint8_t i = 0, c = 0;
	
	for(i = 0; i < len; i++)
	{
		if (gpsStr[i] == '*')
		{
			c++;
		}
	}
	
	return c;
}



void parseGPSStr(uint8_t* gpsStr, struct gpsParsed * dataOut)
{
	volatile uint32_t s = 0,i = 0,j = 0,w = 0,h = 0,k = 0, l = 0;
	_Bool validChar, isGPRMC, isGPVTG;
	uint8_t head[HEAD_LEN] = "";
		
	//Temporary storage structs
	struct gpsRMC rmc = {0};
	struct gpsVTG vtg = {0};

	//Initialize with sentence count zero
	s = 0;
	i = 0;
	
	//Initialize with sentence iterator zero
	while(s < 2)
	{
		//Zero the head counter
		h = 0;
		//Initialize with sentence iterator zero
		while(gpsStr[i] != '*')
		{
			if (gpsStr[i] == '$')
			{
				validChar = 1;
			}	
			else if (gpsStr[i] == ',')
			{
				//Iterate the word counter
				k = 0;
				w++;
			}
			else if (h < HEAD_LEN)
			{
				//Assign the head characters
				head[h] = gpsStr[i];
				//Header iterator
				h++;
			}	
			else if (validChar == 1)
			{
				//We are now parsing either the first or second sentence		
				//First sentence			
				isGPRMC = charArrayCompare(head, GPRMC, HEAD_LEN);
				isGPVTG = charArrayCompare(head, GPVTG, HEAD_LEN);
				
				if (isGPRMC)
				{
					switch(w) 
					{
						case 1 :
							rmc.UTC_Time[k] = gpsStr[i];
							break;
						case 2 :
							rmc.Status[k] = gpsStr[i];
							break;
						case 3 :
							rmc.Latitude[k] = gpsStr[i];
							break;
						case 4 :
							rmc.NS_Indicator[k] = gpsStr[i];
							break;
						case 5 :
							rmc.Longitude[k] = gpsStr[i];
							break;
						case 6 :
							rmc.EW_Indicator[k] = gpsStr[i];
							break;
						case 7 :
							rmc.Speed_Over_Ground[k] = gpsStr[i];
							break;
						case 8 :
							rmc.Course_Over_Ground[k] = gpsStr[i];
							break;						
						case 9 :
							rmc.Date[k] = gpsStr[i];
							break;
						case 10 :
							rmc.Magnetic_Variation[k] = gpsStr[i];
							break;
						case 11 :
							rmc.Magnetic_Variation_B[k] = gpsStr[i];
							break;
						case 12 :
							rmc.Mode[k] = gpsStr[i];
							break;
						default :
							 
							break;
					}	
					k++;
				}
				
				//Second sentence	
				else if (isGPVTG)
				{
					switch(w) 
					{
						case 1 :
							vtg.Course_1[k] = gpsStr[i];
							break;
						case 2 :
							vtg.Reference_1[k] = gpsStr[i];
							break;
						case 3 :
							vtg.Course_2[k] = gpsStr[i];
							break;
						case 4 :
							vtg.Reference_2[k] = gpsStr[i];
							break;
						case 5 :
							vtg.Speed_Knots[k] = gpsStr[i];
							break;
						case 6 :
							vtg.Units_Knots[k] = gpsStr[i];
							break;
						case 7 :
							vtg.Speed_KM[k] = gpsStr[i];
							break;
						case 8 :
							vtg.Units_KM[k] = gpsStr[i];
							break;
						case 9 :
							vtg.Mode[k] = gpsStr[i];
							break;					
						default :
							break;
					}	
					k++;
				}
			}
			i++;
			if (i >= 140)
			{
				return;
			}
		}
		//Start a new sentence
		if (s == 0)
		{
			while(gpsStr[i] != '$')
			{
				i++;
			}
		}
		w = 0;
		validChar = 0;
		s++;
	}
	
	//Store zeros if there is no useful data to be transmitted
	if (vtg.Speed_KM[0] != '0')
	{
		copyArray (vtg.Speed_KM, dataOut->velocity,(uint8_t)5);
		dataOut->velocity_size = countBytes(vtg.Speed_KM, MAX_SIZE_WORD);
	}
	else
	{
		while (l < 2)
			dataOut->velocity[l++] = '0';
		dataOut->velocity[l++] = '.';
		while (l < 5)
			dataOut->velocity[l++] = '0';
		dataOut->velocity_size = l;
		l = 0;
	}
	
	if (rmc.Longitude[0] != 0x00)
	{
		copyArray (rmc.Longitude, dataOut->longitude,(uint8_t)9);
		dataOut->longitude_size = countBytes(rmc.Longitude, MAX_SIZE_WORD);
	}
	else
	{
		while (l < 4)
			dataOut->longitude[l++] = '0';
		dataOut->longitude[l++] = '.';
		while (l < 9)
			dataOut->longitude[l++] = '0';
		dataOut->longitude_size = l;
		l = 0;
	}	
	
	if (rmc.Latitude[0] != 0x00)
	{
		copyArray (rmc.Latitude, dataOut->lattitude,(uint8_t)9);
		dataOut->lattitude_size = countBytes(rmc.Latitude, MAX_SIZE_WORD);
	}
		else
	{
		while (l < 4)
			dataOut->lattitude[l++] = '0';
		dataOut->lattitude[l++] = '.';
		while (l < 9)
			dataOut->lattitude[l++] = '0';
		dataOut->lattitude_size = l;
		l = 0;
	}
	
	if (rmc.UTC_Time[0] != 0x00)
	{
		copyArray (rmc.UTC_Time, dataOut->time,(uint8_t)10);
		dataOut->time_size = countBytes(rmc.UTC_Time, MAX_SIZE_WORD);
	}
	else
	{
		while (l < 6)
			dataOut->time[l++] = '0';
		dataOut->time[l++] = '.';
		while (l < 10)
			dataOut->time[l++] = '0';
		dataOut->time_size = l;
		l = 0;
	}
	if (rmc.Course_Over_Ground[0] != 0x00)
	{
		copyArray (rmc.Course_Over_Ground, dataOut->heading,(uint8_t)6);
		dataOut->heading_size = countBytes(rmc.Course_Over_Ground, MAX_SIZE_WORD);
	}
	else
	{
		while (l < 3)
			dataOut->heading[l++] = '0';
		dataOut->heading[l++] = '.';
		while (l < 6)
			dataOut->heading[l++] = '0';
		dataOut->heading_size = l;
		l = 0;
	}
	
	if (rmc.NS_Indicator[0] != 0x00)
	{
		dataOut->direction[0] = rmc.NS_Indicator[0];
	}
	else
	{
		dataOut->direction[0] = 0x30;
	}
	
	if (rmc.EW_Indicator[0] != 0x00)
	{
		dataOut->direction[1] = rmc.EW_Indicator[0];
	}	
	else
	{
		dataOut->direction[1] = 0x30;
	}
	
	//The direction will always be two bytes
	dataOut->direction_size = 2;
		
}


uint8_t countBytes(uint8_t * inStr, uint32_t maxSize)
{
	uint8_t i;
	
	for (i = 0; i < maxSize; i++)
	{
		if (inStr[i] == 0x00)
		{
			return i;
		}
	}
	
	return maxSize;	
}

