#include "sd_card.h"
#include "usart.h"
#include "lcd.h"
#include "string.h"

void sd_init ()
{
	uint8_t headerCheck[10], escape = 0, loopCount = 0, issues = 0;
	
	clearArraySized(headerCheck, 10);

	do{
		loopCount++;
		issues = sd_append_mode();
		if (issues == 0)
		{
			escape = 1;
		}
		else if (loopCount > 3)
		{
			return;
		}
	} while(!escape);
	escape = 0;
	loopCount = 0;
			
	HAL_UART_Transmit (&huart3, append_header_front, 334, 1000);
			
	do
	{
		loopCount++;
		issues = sd_command_mode();
		if (issues == 0)
		{
			escape = 1;
		}
		else if (loopCount > 3)
		{
			return;
		}
	}while(!escape);
	
}


uint8_t sd_reset(void)
{
	uint32_t start = 0;
	uint8_t escape = 0;
	
	char recieved[100];
	memset(&recieved[0], 0, sizeof(recieved));
	
	uint8_t issue = 0;
	
	HAL_UART_Receive_DMA(&huart3, (uint8_t *)recieved, 100);
	
	HAL_GPIO_WritePin(GPIOD, SD_CARD_DTR_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOD, SD_CARD_DTR_Pin, GPIO_PIN_SET);
	
	start = HAL_GetTick();
	while(!escape)
	{
		if (strncmp(recieved, "12>", 3) == 0) // Connection is good
		{
			escape = 1;
		}
		else if (strncmp(recieved, "1Error card.init", 16) == 0) // Card not inserted
		{
			issue = 1;
			escape = 1;
		}
		else if((HAL_GetTick() - start) > 2000) // Reset Timeout
		{
			issue = 2;
			escape = 1;
		}
	}
	
	HAL_UART_DMAStop(&huart3);
	
	return issue;
}

unsigned sd_file_size(void)
{

	int multiplier = 1;
	int i = 2;
	int length = 0;
	int actual_size = 0;
	uint32_t start = 0;
	uint8_t escape = 0;
	
	char recieved[100];
	char size_collected[100];
	memset(&recieved[0], 0, sizeof(recieved));
	
	uint8_t issue = 0;
	
	HAL_UART_Receive_DMA(&huart3, (uint8_t *)recieved, 100);
	
	HAL_UART_Transmit(&huart3, sd_size, strlen((char*)sd_size), 1000);
	
	HAL_Delay(100);
	
	start = HAL_GetTick();
	while(!escape)
	{
		if (strncmp(recieved, "\r\n", 2) == 0) // Command Went Through
		{
			escape = 1;
		}
		else if((HAL_GetTick() - start) > 2000) // Reset Timeout
		{
			issue = 1;
			escape = 1;
		}
	}
	
	if (issue == 0)
	{
		while(recieved[i] != '\r')
		{
			size_collected[i-2] = recieved[i];
			i++;
		}
		
		size_collected[i-2] = '\0';
		
		length = strlen(size_collected);
		
		for(i = (length-1); i >= 0; i--)
		{		
			actual_size += (size_collected[i] - '0') * multiplier;
			multiplier *= 10;
		}
	}
	
	HAL_UART_DMAStop(&huart3);
	
	return actual_size;
}

uint8_t sd_append_mode()
{
	uint32_t start = 0;
	uint8_t escape = 0;
	
	char recieved[100];
	memset(&recieved[0], 0, sizeof(recieved));
	
	uint8_t issue = 0;
	
	HAL_UART_Receive_DMA(&huart3, (uint8_t *)recieved, 100);
	HAL_UART_Transmit(&huart3, sd_append_data, strlen((char*)sd_append_data),
			                1000);

	start = HAL_GetTick();
	while(!escape)
	{
		if (strncmp(recieved, "\r\n<", 3) == 0) // Command Went Through
		{
			escape = 1;
		}
		else if((HAL_GetTick() - start) > 2000) // Reset Timeout
		{
			issue = 1;
			escape = 1;
		}
	}
	
	HAL_UART_DMAStop(&huart3);
	
	return issue;
}

uint8_t sd_command_mode(void)
{
	uint32_t start = 0;
	uint8_t escape = 0;
	
	char recieved[100];
	memset(&recieved[0], 0, sizeof(recieved));
	
	uint8_t issue = 0;
	
	HAL_UART_Receive_DMA(&huart3, (uint8_t *)recieved, 100);
	
	HAL_UART_Transmit(&huart3, sd_command, charArrayCount(sd_command) - 1, 1000);
	
	start = HAL_GetTick();
	while(!escape)
	{
		if (strncmp(recieved, "~>", 2) == 0) // Command Went Through
		{
			escape = 1;
		}
		else if((HAL_GetTick() - start) > 2000) // Reset Timeout
		{
			issue = 1;
			return issue;
		}
	}
	
	HAL_UART_DMAStop(&huart3);
	
	return issue;
}

//uint8_t sdWriteFrontRear (struct frontInfo *front, struct rearInfo *rear)
//{
//	uint8_t sdReceiveData[100], intToStr[10];
//	uint8_t static message[700];
//	uint8_t issues = 0, escape = 0, loopCount = 0;
//	uint16_t rpm;
//	unsigned i = 0, j = 0, k, l, charCount = 0;
//	
//	
//	
//	clearArraySized (sdReceiveData, 100);
//	
//	if (rear->brake == 0)
//		message[i++] = 'Y';
//	else
//		message[i++] = 'N';
//	message[i++] = ',';
//	
//	message[i++] = rear->gear;
//	message[i++] = ',';
//	
//	charCount = copyNumToString (rear->fuelLevel, intToStr, 0);	
//	while (j < charCount)
//		message[i++] = intToStr[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	charCount = copyNumToString (rear->engineTemp, intToStr, 0);
//	while (j < charCount)
//		message[i++] = intToStr[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	charCount = copyNumToString (rear->cvtTemp, intToStr, 0);
//	while (j < charCount)
//		message[i++] = intToStr[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	rpm = rear->rpmLevel * 60;
//	charCount = copyNumToString (rpm, intToStr, 0);
//	while (j < charCount)
//		message[i++] = intToStr[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	charCount = copyNumToString (rear->batLevel, intToStr, 0);
//	while (j < charCount)
//		message[i++] = intToStr[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	if ( front->sizeInfo.velocitySize < 8)
//	{
//		while (j < front->sizeInfo.velocitySize)
//			message[i++] = front->velocity[j++];
//	}
//	else 
//		message[i++] = '0';
//	message[i++] = ',';
//	j = 0;
//	
//	while (j < front->sizeInfo.longitudeSize)
//		message[i++] = front->longitude[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	while (j < front->sizeInfo.lattitudeSize)
//		message[i++] = front->lattitude[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	while (j < front->sizeInfo.timeSize)
//		message[i++] = front->time[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	while (j < front->sizeInfo.headingSize)
//		message[i++] = front->heading[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	while (j < front->sizeInfo.directionSize)
//		message[i++] = front->direction[j++];
//	message[i++] = ',';
//	j = 0;
//	
//	for (l = 0; l < 4; l++)
//	{
//		for (k = 0; k < 13; k++)
//		{
//			if (l == 0)
//			{
//				charCount = copyNumToString (front->accel[k].data_X, intToStr, 1);
//				while (j < charCount)
//					message[i++] = intToStr[j++];
//				message[i++] = ',';
//				j = 0;
//			}
//			else if (l == 1)
//			{
//				charCount = copyNumToString (front->accel[k].data_Y, intToStr, 1);
//				while (j < charCount)
//					message[i++] = intToStr[j++];
//				message[i++] = ',';
//				j = 0;
//				
//			}
//			else if (l == 2)
//			{
//				charCount = copyNumToString (front->accel[k].data_Z, intToStr, 1);
//				while (j < charCount)
//					message[i++] = intToStr[j++];
//				message[i++] = ',';
//				j = 0;
//			}
//			else if (l == 3)
//			{
//				charCount = copyNumToString (front->accel[k].data_Temp, intToStr, 0);
//				while (j < charCount)
//					message[i++] = intToStr[j++];
//				message[i++] = ',';
//				j = 0;
//			}
//		}
//	}
//	message[i++] = '\r';
//	message[i++] = '\0';
//	
//	do{
//		loopCount++;
//		issues = sd_append_mode();
//		if (issues == 0)
//		{
//			escape = 1;
//		}
//		else if (loopCount > 3)
//		{
//			return 1;
//		}
//	} while(!escape);
//	escape = 0;
//	loopCount = 0;
//	
//	HAL_UART_Transmit(&huart3, message, i, 50);
//	
//	do{
//	loopCount++;
//	issues = sd_command_mode();
//	if (issues == 0)
//	{
//		escape = 1;
//	}
//	else if (loopCount > 3)
//	{
//		return 2;
//	}
//	}while(!escape);
//	
//	return issues;
//}

unsigned copyNumToString (uint16_t num, uint8_t *buffer, uint8_t sign)
{
	int i = 0, j = 0, digit, print = 0, charCount = 0;
	uint8_t ascii;
	
	// make sure buffer is empty before writing
	clearArraySized (buffer, 10);
	
	if ( num == 0)
	{
		buffer[0] = '0';
		return 1;
	}
	
	//if the value is signed, check for negative
	if (sign == 1)
	{
		if ((num & 0x8000) == 0x8000)
		{
			num = ~(num)+1;
			buffer[j++] = '-';
			charCount++;
		}
	}
		
		
	for (i = MAGNITUDE_OF_UINT16; i >= 0; i--)
	{
		// We single out the MSB in question
		digit = (num / power (10, i));
		num %= power (10, i);
		// Then check to see if it should be printed
		if ((print != 0) || (digit != 0))
		{
			charCount++;
			print = 1;
			ascii = digit + NUMERIC_LOWER_LIMIT;
			buffer[j++] = ascii;
		}
	}
	return charCount;
		
}



