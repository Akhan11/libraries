#include "IMU.h"
#include "spi.h"



void initLSM6DS3 ( unsigned imuPort)
{
	static uint8_t returnVar;
	
	// default settings for the IMU
	volatile lsmSetup info = {XYZ, BW_400, G_16, SAMPLE_52, DPS_1000, 
		                             SAMPLE_6660 };
	
	/////////////////////
	//General IMU
	////////////////////
	// This loop will confirm that we are able to read from the IMU
	// This read address should always return 0x69.
	do
	{
		returnVar = readData (imuPort, WHO_AM_I);
	} while (returnVar != I_AM_ME);
	
	// Set the orientation of the IMU
	do
	{
		writeData (imuPort, ORIENT_CFG_G, info.orientation);
		returnVar = readData (imuPort, ORIENT_CFG_G);
	}while (returnVar != info.orientation);
	
	
	/////////////////////
	//Accel
	////////////////////
	do
	{
		writeData (imuPort, CTRL1_XL , ((info.aSample << 4) | (info.aFS << 2) | (info.aFilter)));
		returnVar = readData (imuPort, CTRL1_XL);
	}while (returnVar != ((info.aSample << 4) | (info.aFS << 2) | (info.aFilter)));
	
	do
	{
		writeData (imuPort, CTRL9_XL, ENABLE_XYZ);	
		returnVar = readData (imuPort, CTRL9_XL);
	}while (returnVar != ENABLE_XYZ);
	
	
}

axis readAccel (unsigned imuPort)
{
	static axis temp;
	
	temp.data_X = readData (imuPort, OUTX_H_XL) << 8;
	temp.data_X |= readData (imuPort, OUTX_L_XL);
	
	temp.data_Y = readData (imuPort, OUTY_H_XL) << 8;
	temp.data_Y |= readData (imuPort, OUTY_L_XL);
	
	temp.data_Z = readData (imuPort, OUTZ_H_XL) << 8;
	temp.data_Z |= readData (imuPort, OUTZ_L_XL);
	
	temp.data_Temp = readData (imuPort, OUT_TEMP_H) << 8;
	temp.data_Temp |= readData (imuPort, OUT_TEMP_L);
	
	temp.data_Temp = DEFAULT_TEMP + (temp.data_Temp/16);
	
	
	return temp;
}


axis readGyro (unsigned imuPort)
{
	axis temp;
	
	temp.data_X = readData (imuPort, OUTX_L_G);
	temp.data_X = readData (imuPort, OUTX_H_G) << 8;
	
	temp.data_Y = readData (imuPort, OUTY_H_G);
	temp.data_Y = readData (imuPort, OUTY_L_G) << 8;
	
	temp.data_Z = readData (imuPort, OUTZ_H_G);
	temp.data_Z = readData (imuPort, OUTZ_L_G) << 8;
	
	return temp;
}


axis readMag (unsigned imuPort)
{
	axis temp;
	
	temp.data_X = readData (imuPort, OUTX_L_M);
	temp.data_X = readData (imuPort, OUTX_H_M) << 8;
	
	temp.data_Y = readData (imuPort, OUTY_H_M);
	temp.data_Y = readData (imuPort, OUTY_L_M) << 8;
	
	temp.data_Z = readData (imuPort, OUTZ_H_M);
	temp.data_Z = readData (imuPort, OUTZ_L_M) << 8;
	
	return temp;
}


int16_t readTemp (unsigned imuPort)
{
	int16_t temp;
	
	temp = readData (imuPort, OUT_TEMP_L);
	temp = readData (imuPort, OUT_TEMP_H) << 8;
	
	return temp;
}


uint8_t readData (unsigned imuPort, uint8_t readAddress)
{
	uint8_t rxBuffer;
	readAddress |= 0x80;
	
	HAL_GPIO_WritePin (GPIOD, imuPort, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&hspi3, &readAddress, 1, 10);
	HAL_SPI_Receive (&hspi3, &rxBuffer, 1, 10);
	HAL_GPIO_WritePin (GPIOD, imuPort, GPIO_PIN_SET);
	
	return rxBuffer;
}


void writeData (unsigned imuPort, uint8_t writeAddress,
	              uint8_t writeValue)
{
	uint8_t txBuffer[2] = { writeAddress, writeValue };
	HAL_GPIO_WritePin (GPIOD, imuPort, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&hspi3, txBuffer, 2, 10);
	HAL_GPIO_WritePin (GPIOD, imuPort, GPIO_PIN_SET);
}




