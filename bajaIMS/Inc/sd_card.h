 /** 
  \file IMU.h
  \brief A library for interfacing an SD card R/W

  Author: Matt Koshman
	
  This file will host all relevant functions and defines for interfacing the
  SD card reader/writer
  
	Below the datasheet information you will find a list of all relevant defines,
  and functions used.
	
*/

#include "stdint.h"
#include "bajaIMS.h"


#ifndef SD_CARD_H
#define SD_CARD_H

#define totalDataFields 14

static uint8_t sd_append_data[] = "append car_data.csv\r";


static uint8_t append_header_front[] = "Brake,Gear,Fuel,Engine,CVT,RPM,Battery,"
"Velocity,Longitude,Lattitude,Time,Heading,"
"Direction,1X1,1X2,1X3,1X4,1X5,1X6,1X7,1X8,1X9,1X10,1X11,1X12,1X13,"
"1Y1,1Y2,1Y3,1Y4,1Y5,1Y6,1Y7,1Y8,1Y9,1Y10,1Y11,1Y12,1Y13,"
"1Z1,1Z2,1Z3,1Z4,1Z5,1Z6,1Z7,1Z8,1Z9,1Z10,1Z11,1Z12,1Z13,"
"1AT1,1AT2,1AT3,1AT4,1AT5,1AT6,1AT7,1AT8,1AT9,1AT10,1AT11,1AT12,1AT13\r\0";

static uint8_t sd_size[] = "size car_data.csv\r";
static uint8_t sd_command[4] = {0x1A,0x1A,0x1A,'\0'};


/**
* @brief Inits the sd card for use as a storage device
* @param None
* @retval None
*/
void sd_init (void);


uint8_t sd_reset(void);

void sd_read_car_data(void);

uint8_t sd_append_mode(void);

uint8_t sd_command_mode(void);

unsigned sd_file_size(void);

//uint8_t sdWriteFrontRear (struct frontInfo*, struct rearInfo*);
unsigned copyNumToString (uint16_t num, uint8_t *buffer, uint8_t sign);

#endif
