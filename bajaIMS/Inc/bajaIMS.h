#include <stdint.h>
#include "gps.h"

#ifndef BAJAIMS_H
#define BAJAIMS_H


#define BAT_LEVEL                               (uint16_t)(0x424C)
#define CTEMP                                   (uint16_t)(0x4354)  
#define ETEMP                                   (uint16_t)(0x4554)  
#define RPM                                     (uint16_t)(0x5250)
#define BRAKE                                   (uint16_t)(0x4253)
#define GEAR                                    (uint16_t)(0x4753)
#define FUEL_LEVEL                              (uint16_t)(0x464C)
#define ONE_X                                   (uint16_t)(0x3158)
#define ONE_Y                                   (uint16_t)(0x3159)
#define ONE_Z                                   (uint16_t)(0x315A)
#define P_X                                     (uint16_t)(0x505A)
#define P_Y                                     (uint16_t)(0x505A)
#define P_Z                                     (uint16_t)(0x505A)
#define ONE_TEMP                                (uint16_t)(0x4154)
#define PRINTNEUTRAL                            (uint8_t) (0x4E)
#define PRINTREVERSE                            (uint8_t) (0x52)
#define PRINTDRIVE                              (uint8_t) (0x44)
#define MAX_SIZE_PACKET 0x10
#define MAX_PIT_TRANSMIT                        (uint8_t) (100)
#define PIT_REQUEST_LENGTH                      (uint8_t) (12)
const static uint8_t pitRequest[] = "SEND DATAEND";


typedef struct
{ int16_t data_X, data_Y, data_Z, data_Temp; } Axis;

struct rearInfo
{
	uint16_t gear;
	uint16_t brake;
	uint16_t fuelLevel;
	uint16_t engineTemp;
	uint16_t cvtTemp;
	uint16_t rpmLevel;
	uint16_t batLevel;
};

struct PitMessage
{
	uint8_t message[MAX_PIT_TRANSMIT];
	uint8_t size;
};

typedef enum
{
	FILLER,
	NEUTRAL = 0x4E,
	REVERSE = 0x52,
	DRIVE = 0x44,
} GEAR_STATE;

typedef enum
{
	BRAKE_RELEASED,
	BRAKE_ENGAGED,
} BRAKE_STATE;



unsigned charArrayCompare (uint8_t* str1, const uint8_t* str2, unsigned size);
unsigned charArrayCount(uint8_t* str);
void copyArray (uint8_t* str1, uint8_t* str2, unsigned byteLen);
void clearArraySized (uint8_t* str, uint32_t arrSize);




#endif


