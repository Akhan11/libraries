 /** 
  \file lcd.h
  \brief A library for interfacing a character LCD display
  \note This library is specifically made for the Hitachi 44780 make sure to
        to check the compatibility when using with other character LCDs

  Author:  Matt Koshman / Arsalan Khan
	
  This file will host all relevant functions and declerations to asure the 
  proper usage of the Hitachi 44780 character LCD. All values that are declared
  for use with this character lcd can be found within its datasheet (given below).
  
	Below the datasheet information you will find a list of all relevant defines,
  and functions used.
	
	
  Datasheet : https://koscis.files.wordpress.com/2014/09/hd44780.pdf
              https://www.sparkfun.com/datasheets/LCD/HD44780.pdf

  **** 
  It should be noted that the listings here are just for reference, and values are
  explained where they are declared.
  ****

  Legend:
    Function Macros: Macros assigned for setting certain LCD GPIO pins high/low
    LCD Specific Commands: 8 bit values used to send specific commands to the LCD
                           commands are explained where initialized
    Range Specifiers: Used to identify upper and lower bounds of ASCII characters
    System Messages: Pre-defined alert messages for the driver
		
  Defined Macros:
    (Function Macros) ----- (LCD Specific Commands) ---- (Range Specifiers)
    LCD_ENA_HIGH      ----- LCD_8B2L                ----- ALPHA_LOWER_LIMIT
    LCD_ENA_LOW       ----- LCD_DCB                 ----- ALPHA_MIDDLE_LIMIT
    LCD_RW_HIGH       ----- LCD_MCR                 ----- ALPHA_UPPER_LIMIT
    LCD_RW_LOW        ----- LCD_CLR                 ----- ALPHA_CAP_SHIFT
    LCD_RS_HIGH       ----- LCD_LN1                 ----- NUMERIC_LOWER_LIMIT
    LCD_RS_LOW        ----- LCD_LN2                 ----- NUMERIC_UPPER_LIMIT
                      -----                         ----- VALID_ASCII_LOWER
                      -----                         ----- VALID_ASCII_UPPER
                      -----                         ----- MAGNITUDE_OF_UINT16
								 
    (System Messages)
    ALERT
    RETURN_PIT
    SEND_IT
    ONE_MORE
    GG
    STARS
    MAD
    BAJA
    MONITORING
    SYSTEM
		LOW_FUEL

  Functions (listed in order of apperance):
    lcdDataLoad
    lcdInit
    commandToLCD
    dataToLCD
    clearRowToColLCD
    curserLCD
    clearLCD
		
    printSpeed
    specCharInit
    printBlank
    printZero
    printOne
    printTwo
    printThree
    printFour
    printFive
    printSix
    printSeven
    printEight
    printNine
    fuelSetup
		
    printFuelLevel
		
    clearInfoArea
    printAlert
    printScreen
    printLcdFront
    printLcdRear
		
    speedCalc
    printUint
    power
    checkNumeric
    checkAlphaLow
    checkAlphaCap
    checkAscii
    printString

*/

#ifndef LCD_H
#define LCD_H

#include "bajaIMS.h"

// Defines which indicate different ascii ranges
#define ALPHA_LOWER_LIMIT        0x41 /**< Start of lower case alphas */
#define ALPHA_MIDDLE_LIMIT       0x61 /**< End of lower case alphas & start of upper */
#define ALPHA_UPPER_LIMIT        0x7A /**< End of upper case alphas */
#define ALPHA_CAP_SHIFT          0x20 /**< Shift required to make lower -> upper alpha */
#define NUMERIC_LOWER_LIMIT      0x30 /**< Start of valid numerics */
#define NUMERIC_UPPER_LIMIT      0x39 /**< End of valid numerics */
#define VALID_ASCII_LOWER        0x20 /**< Start of valid ascii character*/
#define VALID_ASCII_UPPER        0x7F /**< End of valid ascii character */
#define MAGNITUDE_OF_UINT16      (uint8_t)(5) /**< a uint16 can have a size of 5 */
#define FULL_FUEL                (uint8_t)(18) /**< Character space required to display full tank*/
#define EMPTY_FUEL               (uint8_t)(0) /**< Character space required to display empty tank*/

//Commands for Hitachi 44780 compatible LCD controllers
#define LCD_8B2L      0x38 /**< Enable 8 bit data, 2 display lines*/ 
#define LCD_DCB       0x0C /**< Enable Display, Cursor, Blink*/ 
#define LCD_MCR       0x06 /**< Set Move Cursor Right*/
#define LCD_CLR       0x01 /**< Home and clear LCD*/
#define LCD_LN1       0x80 /**< Set DDRAM to start of line 1*/
#define LCD_LN2       0xC0 /**< Set DDRAM to start of line 2*/
 
/**
* Function macros to toggle certain pins attached to the LCD
* LCD_ENA_HIGH : Set the ENA pin high
* LCD_ENA_LOW  : Set the ENA pin low
* LCD_RW_HIGH  : Set the RW pin high
* LCD_RW_LOW   : Set the RW pin low
* LCD_RS_HIGH  : Set the RS pin high
* LCD_RS_LOW   : Set the RS pin low
*/
#define LCD_ENA_HIGH (HAL_GPIO_WritePin(GPIOE, LCD_ENA_Pin, GPIO_PIN_SET))
#define LCD_ENA_LOW (HAL_GPIO_WritePin(GPIOE, LCD_ENA_Pin, GPIO_PIN_RESET))
#define LCD_RW_HIGH (HAL_GPIO_WritePin(GPIOE, LCD_RW_Pin, GPIO_PIN_SET))
#define LCD_RW_LOW (HAL_GPIO_WritePin(GPIOE, LCD_RW_Pin, GPIO_PIN_RESET))
#define LCD_RS_HIGH (HAL_GPIO_WritePin(GPIOE, LCD_RS_Pin, GPIO_PIN_SET))
#define LCD_RS_LOW (HAL_GPIO_WritePin(GPIOE, LCD_RS_Pin, GPIO_PIN_RESET))


// Custom messages to be displayed on the LCD
const static uint8_t ALERT[] = "****ALERT****";
const static uint8_t RETURN_PIT[] = "RETURN TO PIT";
const static uint8_t SEND_IT[] = "SEND IT";
const static uint8_t ONE_MORE[] = "ONE MORE LAP";
const static uint8_t GG[] = "PROBLEM";
const static uint8_t STARS[] = "*************";
const static uint8_t MAD[] = "MAD ELECTRONICS";
const static uint8_t BAJA[] = "BAJA: INFORMATION";
const static uint8_t MONITORING[] = "MONITORING";
const static uint8_t SYSTEM[] = "SYSTEM";
const static uint8_t LOW_FUEL[] = "LOW FUEL";

/**
* @brief Given an 8 bit value, send it to the LCD
* @param data: Data to send to the LCD
* @retval None
*/
void lcdDataLoad(uint8_t data);

/**
* @brief Initializes LCD display for use
* @note Refer to datasheet for init process
* @param None
* @retval None
*/
void lcdInit(void);

/**
* @brief Generates control timing and data signals to send a one byte command
         to the LCD
* @note The data value passed in should be a valid LCD command value
* @param data : data value to be send to the LCD
* @retval None
*/
void commandToLCD(uint8_t data);

/**
* @brief Sends a character to be displayed on the LCD
* @note Should be a valid ascii character for proper display
* @param data : character to be printed
* @retval None
*/
void dataToLCD(uint8_t data);

/**
* @brief Clears an area on the indicated row from col1 to col2
* @important row cannot be larger than 4 and smaller than 0
* @important col1 & col2 cannot be larger than 19 and smaller than 0
* @param row : Row location
* @param col1: Starting column location
* @param col2: Ending colum location
* @retval None
*/
void clearRowToColLCD(uint8_t row, uint8_t col1, uint8_t col2);

/**
* @brief Sets the LCD curser to location indicated by a row/column value
* @important row cannot be larger than 4 and smaller than 0
* @important col cannot be larger than 19 and smaller than 0
* @param row : Row location
* @param col: Column location
* @retval None
*/
void curserLCD(uint8_t row, uint8_t col);

/**
* @brief Clears the entire LCD display
* @param None
* @retval None
*/
void clearLCD(void);


/**
* @brief This function displays the speed in special larger characters on the LCD
* @important speed should not be greater than 99 and lower than 0
* @param speed: Speed to display on LCD
* @retval None
*/
void printSpeed(uint8_t speed);

/**
* @brief Initializes special characters to use for the speed display
* @param None
* @retval None
*/
void specCharInit(void);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printBlank(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printZero(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printOne(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printTwo(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printThree(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printFour(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printFive(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printSix(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printSeven(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printEight(uint8_t row, uint8_t col);

/**
* @brief Prints a large 3x3 character
* @param row : Row position for starting character printing
* @param col : Column position for starting character printing
* @retval None
*/
void printNine(uint8_t row, uint8_t col);

/**
* @brief Prints out a full fuel tank, upon system start
* @param None
* @retval None
*/
void fuelSetup(void);


/**
* @brief Given a value in precent 0-100% print out the fuel gauge
* @important Should only pass in values between 0-100
* @param value: Percentage remaining in the fuel tank
* @retval None
*/
void printFuelLevel(uint8_t value);

/**
* @brief Clears an information area on the LCD, contains information for
         gear, temp, direction, rpm
* @param None
* @retval None
*/
void clear_info_area(void);


/**
* @brief Given an alert code, display a message to the LCD
* @important This is an unfinished function!
* @param alertCode : indicates what alert to send out.
* @retval None
*/
void print_alert(uint8_t alert_code);


/**
* @brief Given a string representation of speed, convert to a uint8_t
* @param speed : Speed in character form
* @param size  : Number of characters in the speed array
* @retval uint8_t representation of the speed
*/
uint8_t speedCalc (uint8_t* speed, uint8_t size);

/**
* @brief Given a uint16, print it to the LCD as a character
* @param input : Number to be printed to the LCD
* @retval None
*/
void printUint (uint16_t input);

/**
* @brief Determine value of a base raised to the given exponent
* @param base : Base of exponential
* @param exp  : Exponent of exponential
* @retval result of the operation base^exp
*/
int power (int base, int exp);

/**
* @brief Checks if the passed value is a valid ascii numeric
* @param input : Value to be checked
* @retval 0 or 1 depending on validity of check
*/
uint8_t checkNumeric(uint8_t input);

/**
* @brief Checks if the passed value is a valid ascii lower alpha
* @param input : Value to be checked
* @retval 0 or 1 depending on validity of check
*/
uint8_t checkAlphaLow(uint8_t input);

/**
* @brief Checks if the passed value is a valid ascii upper alpha
* @param input : Value to be checked
* @retval 0 or 1 depending on validity of check
*/
uint8_t checkAlphaCap(uint8_t input);

/**
* @brief Checks if the passed value is a valid ascii
* @param input : Value to be checked
* @retval 0 or 1 depending on validity of check
*/
uint8_t checkAscii (uint8_t input);

/**
* @brief Given a string and size, print string to the LCD
* @param str : String to be printed
* @param size  : Size of string to be printed
* @retval None
*/
void printString (uint8_t* str, unsigned size);

void printInfoArea (struct gpsParsed *velocity, uint32_t rpm, uint8_t brake,
                    uint8_t gear, uint32_t fuelLevel, uint8_t messageDisplay,
                    uint8_t *message, uint8_t messageLen, uint32_t eTemp);


#endif
