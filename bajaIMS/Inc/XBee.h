#include "usart.h"

#define DATA_AVAILABLE 0x80
#define CHECK_SUM      0x0D
#define CHECK_E 0x45
#define CHECK_N 0x4E
#define CHECK_D 0x44


// ready receive xxxxx
#define RRF_LEN 5
#define RRR_LEN 4
#define MSTOP_LEN 8
#define MACK_LEN 7
#define MAX_MASTER_TRANSMIT 8

#define SSTOP_LEN 8
#define SLAVE_BYTE_LEN 2
#define MAX_SLAVE_TRANSMIT 300

const static uint8_t RRF[] = "FRONT";
const static uint8_t REQUESTFRONT[] = "FRONTEND";
const static uint8_t REQUESTREAR[] = "REARREND";
const static uint8_t RRR[] = "REAR";

unsigned charArrayCompareXbee (uint8_t*, const uint8_t*, unsigned);
unsigned charArrayCompare (uint8_t*, const uint8_t*, unsigned size);
unsigned charArrayCount(uint8_t* str);
void clearArray (uint8_t* str,  uint16_t dataLen);
void copyArray (uint8_t* str1, uint8_t* str2, unsigned len);
