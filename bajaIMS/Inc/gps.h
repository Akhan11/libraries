 /** 
  \file gps.h
  \brief A library for interfacing a GPS module using USART/UART
  \note This library is specifically made for the FGPMMOPA6C gps module.

  Author: Drake Cornell / Arsalan Khan
	
  This file will host all relevant functions and declerations to asure the 
  proper usage of the FGPMMOPA6C gps.
  
	Below the datasheet information you will find a list of all relevant defines,
  and functions used.
	
	
  Datasheet : https://cdn-shop.adafruit.com/datasheets/
              GlobalTop-FGPMMOPA6C-Datasheet-V0A-Preliminary.pdf

  **** 
  It should be noted that the listings here are just for reference, and values are
  explained where they are declared.
  ****

  Legend:
    Setting Macros: Macros assigned for assigning a mode to the gps module
    General Macros: General macros to remove magic numbers
    
  Defined Macros:
    (Setting Macros) ----- (General Macros) 
    GPRMC            ----- MSG_LEN        
    GPVTG            ----- HEAD_LEN       
    initStrProtocols ----- NULL_CHAR      
    initStrRate      ----- MAX_SIZE_WORD  
                     ----- MAX_SIZE_PACKET
    						 
  Structures:
    gpsSize
    gpsRMC
    gpsVTG
    gpsParsed
    

  Functions (listed in order of apperance):
    copyArrayToStar
    starCount
    parseGPSStr
    countBytes
*/


#include "stdint.h"

#ifndef GPS_H
#define GPS_H

#define MSG_LEN 140
#define HEAD_LEN 5
#define NULL_CHAR 0x00
#define MAX_SIZE_WORD 0x0F
#define MAX_SIZE_PACKET 0x10

const static uint8_t GPRMC[] = "GPRMC";
const static uint8_t GPVTG[] = "GPVTG";
// strings sent to the gps module to assign settings
// refer to data sheet for information on these settings
const static uint8_t initStrProtocols[] = "$PMTK314,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n";
const static uint8_t initStrRate[] = "$PMTK220,200*2C\r\n";

/** 
* struct gpsRMC
* A structure which will hold all RMC information obtained from the gps module
*/
struct gpsRMC
{
	uint8_t UTC_Time[15];
	uint8_t Status[15];
	uint8_t Latitude[15];
	uint8_t NS_Indicator[15];
	uint8_t Longitude[15];
	uint8_t EW_Indicator[15];
	uint8_t Speed_Over_Ground[15];
	uint8_t Course_Over_Ground[15];
	uint8_t Date[15];
	uint8_t Magnetic_Variation[15];
	uint8_t Magnetic_Variation_B[15];
	uint8_t Mode[15];
};

/** 
* struct gpsVTG
* A structure which will hold all VTG information obtained from the gps module
*/
struct gpsVTG 
{
	uint8_t Course_1[15];
	uint8_t Reference_1[15];
	uint8_t Course_2[15];
	uint8_t Reference_2[15];
	uint8_t Speed_Knots[15];
	uint8_t Units_Knots[15];
	uint8_t Speed_KM[15];
	uint8_t Units_KM[15];
	uint8_t Mode[15];
};

/** 
* struct gpsParsed
* A structure which will hold all information relevant to us from the VTG & RMC
* parsed information
*/
struct gpsParsed //gpsParsed
{
	uint8_t velocity[7];
	uint8_t longitude[11];
	uint8_t lattitude[11];
	uint8_t time[12];
	uint8_t heading[8];
	uint8_t direction[4];
	
	uint8_t velocity_size;
	uint8_t longitude_size;
	uint8_t lattitude_size;
	uint8_t time_size;
	uint8_t heading_size;
	uint8_t direction_size;	
	
};

/**
* @brief Given the gps UART stream, copy all valid information between the
         $ and *. Refer to gps output string for more information.
* @param str1 : String that was read from the gps
* @param str2 : String which will store relevant extracted information
* @param len  : Length of str1
* @retval None
*/
void copyArrayToStar (uint8_t* str1, volatile uint8_t* str2, uint32_t len);

/**
* @brief Given a string of extracted gps information, check how many * exist
         to make sure we have the two pecies of information needed
* @param gpsStr    : string extracted from the gps
* @param len       : Length of gpsStr
* @retval uint32_t : number of stars within the input string
* @note We are expecting a return value of two for this use case
*/
uint8_t starCount(uint8_t* gpsStr, uint32_t len);

/**
* @brief Given a string which has been validated by the star count
         this function will parse the string an store valuable information
         into a struct
* @important gpsStr must be validated by starCount first (returns 2)
* @param gpsStr  : string extracted from the gps, starCount validated
* @param dataOut : structure which contains all relevant parse information
* @see struct gpsParsed
* @retval None
*/
void parseGPSStr(uint8_t* gpsStr, struct gpsParsed * dataOut);

/**
* @brief Return the number of bytes within the given string
* @param inStr : Input string to count
* @param maxSize : Maximum size possible for input string
* @retval uint8_t : size of string in bytes
*/
uint8_t countBytes(uint8_t * inStr, uint32_t maxSize);

#endif
