 /** 
  \file IMU.h
  \brief A library for interfacing an IMU
  \note This library is specific for the LSM6DS3 & LSM6DS3TR

  Author: Arsalan Khan
	
  This file will host all relevant register address, typedefs, and fucntions
  for reading & writing values to the IMU
  
	Below the datasheet information you will find a list of all relevant defines,
  and functions used.
	
	
  Datasheet : http://www.st.com/en/mems-and-sensors/lsm6ds3.html

  **** 
  It should be noted that the listings here are just for reference, and values are
  explained where they are declared.
  ****

  Legend:
    General IMU Registers: Macros assigned for setting certain LCD GPIO pins high/low
    Accel Registers:
    Gyro Registers:
    Mag Registers:
    Header Defines:
    
		
  Defined Macros:
    (General IMU Registers) ----- (Accel Registers) ----- (Gyro Registers)
    CTRL_BASE               ----- CTRL1_XL          ----- CTRL2_G
    OUTPUT_BASE             ----- CTRL9_XL          ----- CTRL10_C
    MAG_BASE                ----- OUTX_L_XL         ----- OUTX_L_G
    ORIENT_CFG_G            ----- OUTX_H_XL         ----- OUTX_H_G
    WHO_AM_I                ----- OUTY_L_XL         ----- OUTY_L_G
    I_AM_ME                 ----- OUTY_H_XL         ----- OUTY_H_G
    STATUS_REG              ----- OUTZ_L_XL         ----- OUTZ_L_G
    ENABLE_XYZ              ----- OUTZ_H_XL         ----- OUTZ_H_G
    DEFAULT_TEMP            ----- OUT_TEMP_L        ----- 
                            ----- OUT_TEMP_H        ----- 
								 
    (Mag Registers) ----- (Header Defines)
    OUTX_L_M        ----- ACCEL1_X
    OUTX_H_M        ----- ACCEL1_Y
    OUTY_L_M        ----- ACCEL1_Z
    OUTY_H_M        ----- TEMP
    OUTZ_L_M        ----- GYRO_X
    OUTZ_H_M        ----- GYRO_Y
                    ----- GYRO_Z
                    ----- MAG_X
                    ----- MAG_Y
                    ----- MAG_Z
				 
  Enums:
    IMU_ORIENTATION
    GYRO_FS
    SAMPLE_RATE
    ACCEL_FS
    ACCEL_FILTER
    SENSOR

  Structures:
	  LSM6DS3_SETUP
    axis
	   
  Functions (listed in order of apperance):
    initLSM6DS3
    readAccel
    readGyro
    readMag
    readTemp
    readData
    writeData
*/


#ifndef IMU_H
#define IMU_H

#include <stdint.h>


/**
The following list of defines are referencing registers that exist within the
LSM6DS3. This is an IMU that has an accelerometer, gyro, and magnometer
avaliable. 

Unless otherwise mentioned in a comment, all registers listed below will have a
undefined or 0 initial value
*/
/////////////////////////////////
// LSM6DS3 General Registers
/////////////////////////////////
#define CTRL_BASE        (uint8_t)(0x10)
#define OUTPUT_BASE      (uint8_t)(0x20)
#define MAG_BASE         (uint8_t)(0x66)

#define ORIENT_CFG_G      (uint8_t)(0x0B)
#define WHO_AM_I          (uint8_t)(0x0F) // 0x69
#define I_AM_ME           (uint8_t)(0x69)
#define STATUS_REG        (uint8_t)(0x1E)
#define ENABLE_XYZ        (uint8_t)(0x38)
#define DEFAULT_TEMP      (uint8_t) 25

/////////////////////////////////
// LSM6DS3 Accel Registers
/////////////////////////////////
#define CTRL1_XL       (uint8_t)(CTRL_BASE)
#define CTRL9_XL       (uint8_t)(CTRL_BASE + 0x08) // 0x38
#define OUTX_L_XL      (uint8_t)(OUTPUT_BASE + 0x08)
#define OUTX_H_XL      (uint8_t)(OUTPUT_BASE + 0x09)
#define OUTY_L_XL      (uint8_t)(OUTPUT_BASE + 0x0A)
#define OUTY_H_XL      (uint8_t)(OUTPUT_BASE + 0x0B)
#define OUTZ_L_XL      (uint8_t)(OUTPUT_BASE + 0x0C)
#define OUTZ_H_XL      (uint8_t)(OUTPUT_BASE + 0x0D)

/////////////////////////////////
// LSM6DS3 Temp Registers
/////////////////////////////////
#define OUT_TEMP_L      (uint8_t)(OUTPUT_BASE)
#define OUT_TEMP_H      (uint8_t)(OUTPUT_BASE + 0x01)

/////////////////////////////////
// LSM6DS3 Magnometer Registers
/////////////////////////////////
#define OUTX_L_M      (uint8_t)(MAG_BASE)
#define OUTX_H_M      (uint8_t)(MAG_BASE + 0x01)
#define OUTY_L_M      (uint8_t)(MAG_BASE + 0x02)
#define OUTY_H_M      (uint8_t)(MAG_BASE + 0x03)
#define OUTZ_L_M      (uint8_t)(MAG_BASE + 0x04)
#define OUTZ_H_M      (uint8_t)(MAG_BASE + 0x05)

/////////////////////////////////
// LSM6DS3 Gyro Registers
/////////////////////////////////
#define CTRL2_G       (uint8_t)(CTRL_BASE + 0x01)
#define CTRL10_C      (uint8_t)(CTRL_BASE + 0x09) // 0x38
#define OUTX_L_G      (uint8_t)(OUTPUT_BASE + 0x02)
#define OUTX_H_G      (uint8_t)(OUTPUT_BASE + 0x03)
#define OUTY_L_G      (uint8_t)(OUTPUT_BASE + 0x04)
#define OUTY_H_G      (uint8_t)(OUTPUT_BASE + 0x05)
#define OUTZ_L_G      (uint8_t)(OUTPUT_BASE + 0x06)
#define OUTZ_H_G      (uint8_t)(OUTPUT_BASE + 0x07)

/////////////////////////////////
// Headers for Sending
/////////////////////////////////
#define ACCEL1_X      (uint32_t)(0x31580000)
#define ACCEL1_Y      (uint32_t)(0x31590000)  
#define ACCEL1_Z      (uint32_t)(0x315A0000)  
#define TEMP          (uint32_t)(0x41540000)


// For selecting what the orientation will be for the IMU
// XYZ -> Pitch Roll Yaw, we will be using a default value of
// XYZ or 000, for our project. 
typedef enum
{
	XYZ = 0x0,
	XZY = 0x1,
	YXZ = 0x2,
	YZX = 0x3,
	ZXY = 0x4,
	ZYX = 0x5,
} 
IMU_ORIENTATION;
	
typedef enum 
{
	DPS_250  = 0x0,  // 00 = 250 dps
	DPS_500  = 0x1,  // 01 = 500 dps
	DPS_1000 = 0x2, // 10 = 1000 dps
	DPS_2000 = 0x3, // 11 = 2000 dps
} 
GYRO_FS;

typedef enum 
{
	PWR_OFF      = 0x0,     // 0000 = power down
	SAMPLE_12_5  = 0x1, // 0001 = 12.5 Hz
	SAMPLE_26    = 0x2,   // 0010 = 26   Hz
	SAMPLE_52    = 0x3,   // 0011 = 52   Hz
	SAMPLE_104   = 0x4,  // 0100 = 104  Hz
	SAMPLE_208   = 0x5,  // 0101 = 208  Hz
	SAMPLE_416   = 0x6,  // 0110 = 416  Hz
	SAMPLE_833   = 0x7,  // 0111 = 833  Hz
	SAMPLE_1660  = 0x8, // 1000 = 1660 Hz
	SAMPLE_3330  = 0x9, // 1001 = 3330 Hz
	SAMPLE_6660  = 0xA, // 1010 = 6600 HZ
} 
SAMPLE_RATE;

typedef enum 
{
	G_2   = 0x0,  // 00 = +/- 2g
	G_16  = 0x1, // 01 = +/- 16g
	G_4   = 0x2,  // 10 = +/- 4g
	G_8   = 0x3,  // 11 = +/-8g
} 
ACCEL_FS;


/**
If using the anti-aliasing filter, please follow the table below for BW selection.
The frequencies listed are representative of the Accel sample speed.
6.66 - 3.33 KHz -> No filter
1.66        KHz -> 400 Hz
833          Hz -> 400 Hz
416          Hz -> 200 Hz
208          Hz -> 100 Hz
104 - 10     Hz -> 50  Hz
*/
typedef enum
{
	BW_400 = 0x0, // Anti-Alias filter BW 400 Hz
	BW_200 = 0x1, // Anti-Alias filter BW 200 Hz
	BW_100 = 0x2, // Anti-Alias filter BW 100 Hz
	BW_50  = 0x3,  // Anti-Alias filter BW 50  Hz
} 
ACCEL_FILTER;

typedef enum
{
	ACCEL,
	GYRO,
	MAG,
} 
SENSOR;

//The following is a class for the LSM6DS3
typedef struct 
{
	IMU_ORIENTATION orientation;
	ACCEL_FILTER  aFilter;
	ACCEL_FS      aFS;
	SAMPLE_RATE   aSample;
	GYRO_FS       gFS;
	SAMPLE_RATE   gSample;
} 
lsmSetup;


// axis struct to use for Mag, Accel, and Gyro
typedef struct
{ 
  int16_t data_X, data_Y, data_Z, data_Temp; 
} 
axis;

/**
* @brief  Initialization for the LSM6DS3 chip
* @note   This will normally be a sweeping intialization for every
          LSM6DS3 chip on the board. However you can also configure
	        each chip differently if required
* @param  info : This will contain the intialization data for each IMU
* @param  gyroPort : Indicates which gyro we are trying to configure
* @retval None
*/
void initLSM6DS3 (unsigned imuPort);

/**
* @brief  Read the raw data from the accelerometer on the IMU
* @note   This will only return a raw value, further processing must
          be done to make this is useful value
* @param  imuPort : Indicates which IMU we are trying to read from
                    are trying to write to
* @retval A struct of type axis which contains the raw XYZ values
          from the accelerometer
*/
axis readAccel (unsigned imuPort);

/**
* @brief  Read the raw data from the gryo on the IMU
* @note   This will only return a raw value, further processing must
          be done to make this is useful value
* @param  imuPort : Indicates which IMU we are trying to read from
                    are trying to write to
* @retval A struct of type axis which contains the raw XYZ values
          from the gryo
*/
axis readGyro (unsigned imuPort);

/**
* @brief  Read the raw data from the magnometer on the IMU
* @note   This will only return a raw value, further processing must
          be done to make this is useful value
* @param  imuPort : Indicates which IMU we are trying to read from
                    are trying to write to
* @retval A struct of type axis which contains the raw XYZ values
          from the magnometer
*/
axis readMag (unsigned imuPort);

/**
* @brief  Read the raw data from the temprature sensor on the IMU
* @note   This will only return a raw value, further processing must
          be done to make this is useful value
* @param  imuPort : Indicates which IMU we are trying to read from
                    are trying to write to
* @retval A 16 bit number representing the value of the temprature
*/
int16_t readTemp (unsigned imuPort);

/**
* @brief  Read a value of data from the IMU depending on the bytesize
* @note   You will be able to read 1 byte of memory from the IMU. User
          should only pass readable IMU registers to this function.
          When reading accel, mag, and gyro data vlues this should be
          typecast to int16_t on the return
* @param  imuPort : Indicates which IMU we are trying to read from  
* @param  readAddress : Indicates the the address in the IMU memory we 
                        are trying to read from
* @retval returns 16 byte int of the data read in
*/
uint8_t readData  (unsigned imuPort, uint8_t readAddress);

/**
* @brief  Write a value to the IMU given a write address and data
* @note   You will be able to write 2 bytes of data to the IMU. 
          All data that is sent to the IMU must be intentional
          and allowed in accordance to the data sheet. Some error
          checks will be implemented to prevent data mishandling
* @param  imuPort : Indicates which IMU we are trying to read from
* @param  writeAddress : Indicates the the address in the IMU memory we 
                         are trying to write to
* @param  writeValue : Indicates the value we are writing to the IMU
* @retval None
*/
void writeData (unsigned imuPort, uint8_t writeAddress, uint8_t writeValue);

#endif

