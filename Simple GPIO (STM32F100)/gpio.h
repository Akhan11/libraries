/** \file gpio.h
    \brief A general purpose library for input/output related tasks.
    
    This file contains functions relevant to configure GPIO pins,
    setting/resetting pins, and holding typedefs for pin relevant registers.
		
    Macros:
	1) AFIO_ENABLE
	2) GPIOA_ENABLE
	3) GPIOB_ENABLE
	4) GPIOC_ENABLE
    5) GPIOD_ENABLE
	6) CNF_MODE_FLAG
	
    Enums:
    1) PIN_MODE
    2) PIN_CONFIGURATION
    3) GPIOX_SET_RESET
			
    Functions:
    1) initGeneralClock
    2) setGpioConfig
    3) gpioSetState
		
    Author: Arsalan Khan
    Date Started: April 1, 2018
    Date Finished: April 2, 2018
    
*/
#ifndef GPIO_H
#define GPIO_H
#include "main.h"

#define AFIO_ENABLE                    bit0
#define GPIOA_ENABLE                   bit2
#define GPIOB_ENABLE                   bit3
#define GPIOC_ENABLE                   bit4
#define GPIOD_ENABLE                   bit5


// Mask to enable clearing of CNF/MODE bits before setting
#define CNF_MODE_MASK                  0xf

/** 
* typedef enum PINMODE
* A type definition for values coresponding to MODE configuration
* Includes all valid values for the MODE configuration register.
*/
typedef enum
{
  GPIO_INPUT_MODE = 0x0, /**< Set pin to input mode */
  GPIO_OUTPUT_MODE_10MHZ = 0x1, /**< Set pin to output mode with speed 10 MHZ*/
  GPIO_OUTPUT_MODE_2MHZ = 0x2, /**< Set pin to output mode with speed 2 MHZ*/
  GPIO_OUTPUT_MODE_50MHZ = 0x3, /**< Set pin to output mode with speed 50 MHZ*/
}
PIN_MODE;



/**
* typedef enum PIN_CONFIGUARTION
* A type definition for values coresponding to the CNF configuration
* User must be aware of pin MODE before setting CNF

* Includes all valid values for the CNF configuration register.
* Accounts for input and output variants of the CNF register.
*/
typedef enum
{
  INPUT_CNF_ANALOG_MODE = 0x0, /**< Sets pin to analog mode if in input mode*/
  INPUT_CNF_FLOATING_INPUT_MODE = 0x1, /**< Sets pin floating if in input mode*/
  INPUT_CNF_PUPD_MODE = 0x2, /**< Sets pin to pull up/down if in input mode */
	
  OUTPUT_CNF_GPO_PP = 0x0, /**< Sets pin to general purpose push pull if in output mode*/
  OUTPUT_CNF_GPO_OD = 0x1, /**< Sets pin to general purpose open drain if in output mode*/
  OUTPUT_CNF_AFO_PP = 0x2, /**< Sets pin to alternate function push pull if in output mode*/
  OUTPUT_CNF_AFO_OD = 0x3, /**< Sets pin to alternate function open drain if in output mode*/
}
PIN_CONFIGURATION;


/**
* typedef enum GPIOX_SET_RESET
* type definition to assist with pin setting and resetting
*/
typedef enum
{
  GPIOX_PIN0_SET = bit0, /**< Bit mask for setting pin0 in any GPIO BSRR register*/
  GPIOX_PIN1_SET = bit1, /**< Bit mask for setting pin1 in any GPIO BSRR register*/
  GPIOX_PIN2_SET = bit2, /**< Bit mask for setting pin2 in any GPIO BSRR register*/
  GPIOX_PIN3_SET = bit3, /**< Bit mask for setting pin3 in any GPIO BSRR register*/
  GPIOX_PIN4_SET = bit4, /**< Bit mask for setting pin4 in any GPIO BSRR register*/
  GPIOX_PIN5_SET = bit5, /**< Bit mask for setting pin5 in any GPIO BSRR register*/
  GPIOX_PIN6_SET = bit6, /**< Bit mask for setting pin6 in any GPIO BSRR register*/
  GPIOX_PIN7_SET = bit7, /**< Bit mask for setting pin7 in any GPIO BSRR register*/
  GPIOX_PIN8_SET = bit8, /**< Bit mask for setting pin8 in any GPIO BSRR register*/
  GPIOX_PIN9_SET = bit9, /**< Bit mask for setting pin9 in any GPIO BSRR register*/
  GPIOX_PIN10_SET = bit10, /**< Bit mask for setting pin10 in any GPIO BSRR register*/
  GPIOX_PIN11_SET = bit11, /**< Bit mask for setting pin11 in any GPIO BSRR register*/
  GPIOX_PIN12_SET = bit12, /**< Bit mask for setting pin12 in any GPIO BSRR register*/
  GPIOX_PIN13_SET = bit13, /**< Bit mask for setting pin13 in any GPIO BSRR register*/
  GPIOX_PIN14_SET = bit14, /**< Bit mask for setting pin14 in any GPIO BSRR register*/
  GPIOX_PIN15_SET = bit15, /**< Bit mask for setting pin15 in any GPIO BSRR register*/

  GPIOX_PIN0_RESET = bit16, /**< Bit mask for resetting pin0 in any GPIO BSRR register*/
  GPIOX_PIN1_RESET = bit17, /**< Bit mask for resetting pin1 in any GPIO BSRR register*/
  GPIOX_PIN2_RESET = bit18, /**< Bit mask for resetting pin2 in any GPIO BSRR register*/
  GPIOX_PIN3_RESET = bit19, /**< Bit mask for resetting pin3 in any GPIO BSRR register*/
  GPIOX_PIN4_RESET = bit20, /**< Bit mask for resetting pin4 in any GPIO BSRR register*/
  GPIOX_PIN5_RESET = bit21, /**< Bit mask for resetting pin5 in any GPIO BSRR register*/
  GPIOX_PIN6_RESET = bit22, /**< Bit mask for resetting pin6 in any GPIO BSRR register*/
  GPIOX_PIN7_RESET = bit23, /**< Bit mask for resetting pin7 in any GPIO BSRR register*/
  GPIOX_PIN8_RESET = bit24, /**< Bit mask for resetting pin8 in any GPIO BSRR register*/
  GPIOX_PIN9_RESET = bit25, /**< Bit mask for resetting pin9 in any GPIO BSRR register*/
  GPIOX_PIN10_RESET = bit26, /**< Bit mask for resetting pin10 in any GPIO BSRR register*/
  GPIOX_PIN11_RESET = bit27, /**< Bit mask for resetting pin11 in any GPIO BSRR register*/
  GPIOX_PIN12_RESET = bit28, /**< Bit mask for resetting pin12 in any GPIO BSRR register*/
  GPIOX_PIN13_RESET = bit29, /**< Bit mask for resetting pin13 in any GPIO BSRR register*/
  GPIOX_PIN14_RESET = bit30, /**< Bit mask for resetting pin14 in any GPIO BSRR register*/
  GPIOX_PIN15_RESET,  /**< Bit mask for resetting pin15 in any GPIO BSRR register*/
}
GPIOX_SET_RESET;

/**
* @brief Init all general clocks to be used by the system
* @note All peripheral specific clocks are enabled in their respective
        functions
* @param None
* @retval None
*/
void initGeneralClock (void);

/**
* @brief Set the configuration of a given pin depending on input values
* @note The user will be able to use the pre-defined values to set pins to
*       a valid configuration
* @param port: The port which the pin resides on. (Valid values are A,B,C,D)
* @param pin: The pin that the user would like to configure
* @param mode: The mode which the user would like to use
* @param cnf: The configuartion for the selected mode
* @see PIN_MODE
* @see PIN_CONFIGURATION
* @retval None
*/
void setGpioConfig (char port, uint8_t pin, PIN_MODE mode, PIN_CONFIGURATION cnf);

/**
* @brief Set the state of any give pin & port
* @note The pin to be set/reset will be indicated by the second parameter.
        This parameter is of type GPIOX_SET_RESET
* @param port: The port which the pin resides on. (Valid values are A,B,C,D)
* @param state: The pin to set/reset and its number (of type GPIOX_SET_RESET)
* @see GPIOX_SET_RESET
* @retval None
*/
void gpioSetState (char port, GPIOX_SET_RESET state);

#endif
