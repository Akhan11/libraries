#include "gpio.h"

void initGeneralClock (void)
{
  RCC->APB2ENR |= GPIOC_ENABLE;
}

void setGpioConfig (char port, uint8_t pin, uint8_t mode, uint8_t cnf)
{
  uint8_t configuration = (cnf << 2 | mode);
  if (port == 'A' || port == 'a')
  {
    if ( pin >= 8 && pin <= 15)
    {
      pin -= 8;
      GPIOA->CRH &= ~(CNF_MODE_MASK << (pin*4));
      GPIOA->CRH |= (configuration << (pin*4));
    }
    else if (pin <= 7)
    {
       GPIOA->CRL &= ~(CNF_MODE_MASK << (pin*4));
       GPIOA->CRL |= (configuration << (pin*4));
    }
  }
  else if (port == 'B' || port == 'b')
  {
    if ( pin >= 8 && pin <= 15)
    {
       pin -= 8;
       GPIOB->CRH &= ~(CNF_MODE_MASK << (pin*4));
       GPIOB->CRH |= (configuration << (pin*4));
    }
    else if (pin <= 7)
    {
       GPIOB->CRL &= ~(CNF_MODE_MASK << (pin*4));
       GPIOB->CRL |= (configuration << (pin*4));
    }
  }
  else if (port == 'C' || port == 'c')
  {
    if ( pin >= 8 && pin <= 15)
    {
      pin -= 8;
      GPIOC->CRH &= ~(CNF_MODE_MASK << (pin*4));
      GPIOC->CRH |= (configuration << (pin*4));
    }
    else if (pin <= 7)
    {
      GPIOC->CRL &= ~(CNF_MODE_MASK << (pin*4));
      GPIOC->CRL |= (configuration << (pin*4));
    }	
  }
  else if (port == 'D' || port == 'd')
  {
    if ( pin >= 8 && pin <= 15)
    {
      pin -= 8;
      GPIOD->CRH &= ~(CNF_MODE_MASK << (pin*4));
      GPIOD->CRH |= (configuration << (pin*4));
    }
    else if (pin <= 7)
    {
      GPIOD->CRL &= ~(CNF_MODE_MASK << (pin*4));
      GPIOD->CRL |= (configuration << (pin*4));
     }
  }
	// invalid port
  else
    return;
}

void gpioSetState (char port, GPIOX_SET_RESET state)
{
  uint32_t assign = state;
	
  if (state == GPIOX_PIN15_RESET)
    assign = bit31;
	
  if (port == 'A' || port == 'a')
  {
    GPIOA->BSRR |= assign;
  }
  else if (port == 'B' || port == 'b')
  {
    GPIOB->BSRR |= assign;
  }
  else if (port == 'C' || port == 'c')
  {
    GPIOC->BSRR |= assign;
  }
  else if (port == 'D' || port == 'd')
  {
    GPIOD->BSRR |= assign;
  }
// invalid port
  else
     return;
}


