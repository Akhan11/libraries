#include "stm32f10x.h"

#define bit0                           (uint32_t)(0x1)
#define bit1                           (uint32_t)(0x1 << 1)
#define bit2                           (uint32_t)(0x1 << 2)
#define bit3                           (uint32_t)(0x1 << 3)
#define bit4                           (uint32_t)(0x1 << 4)
#define bit5                           (uint32_t)(0x1 << 5)
#define bit6                           (uint32_t)(0x1 << 6)
#define bit7                           (uint32_t)(0x1 << 7)
#define bit8                           (uint32_t)(0x1 << 8)
#define bit9                           (uint32_t)(0x1 << 9)
#define bit10                          (uint32_t)(0x1 << 10)
#define bit11                          (uint32_t)(0x1 << 11)
#define bit12                          (uint32_t)(0x1 << 12)
#define bit13                          (uint32_t)(0x1 << 13)
#define bit14                          (uint32_t)(0x1 << 14)
#define bit15                          (uint32_t)(0x1 << 15)
#define bit16                          (uint32_t)(0x1 << 16)
#define bit17                          (uint32_t)(0x1 << 17)
#define bit18                          (uint32_t)(0x1 << 18)
#define bit19                          (uint32_t)(0x1 << 19)
#define bit20                          (uint32_t)(0x1 << 20)
#define bit21                          (uint32_t)(0x1 << 21)
#define bit22                          (uint32_t)(0x1 << 22)
#define bit23                          (uint32_t)(0x1 << 23)
#define bit24                          (uint32_t)(0x1 << 24)
#define bit25                          (uint32_t)(0x1 << 25)
#define bit26                          (uint32_t)(0x1 << 26)
#define bit27                          (uint32_t)(0x1 << 27)
#define bit28                          (uint32_t)(0x1 << 28)
#define bit29                          (uint32_t)(0x1 << 29)
#define bit30                          (uint32_t)(0x1 << 30)
#define bit31                          (uint32_t)(0x1 << 31)

void delay(uint32_t delayTime);

