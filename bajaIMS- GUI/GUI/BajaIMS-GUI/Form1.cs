﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Threading;


namespace BajaIMS_GUI
{
    public partial class Form1 : Form
    {

        int totalTimeCSeconds;
        int totalTimeSeconds;
        int totalTimeMinutes;
        int totalTimeHours;
        int lapTimeCSeconds;
        int lapTimeSeconds;
        int lapTimeMinutes;
        int lapTimeHours;
        int lapNumbers;
        int end;
        bool timerOn;

        public System.IO.Ports.SerialPort xbee;
        private readonly ConcurrentQueue<string> _queue = new ConcurrentQueue<string>();
        private readonly AutoResetEvent _signal = new AutoResetEvent(false);

        String comReceive = null;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            resetTimer();
            timerOn = false;
            end = 0;
            foreach (String s in System.IO.Ports.SerialPort.GetPortNames())
            {
                comList.Items.Add(s);
            }
        }

        private void minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void stopReceive_Click(object sender, EventArgs e)
        {
            xbee.Close();
        }

        private void startReceive_Click(object sender, EventArgs e)
        {
            int baudRate = Convert.ToInt32(baudList.Text);
            string comPort = comList.Text;
            timerOn = true;

            serialConnect(comPort, baudRate);
            

        }

        public void serialConnect (string comPort, int baudRate)
        {
            xbee = new System.IO.Ports.SerialPort(comPort, baudRate, System.IO.Ports.Parity.None,
                                                   8, System.IO.Ports.StopBits.One);

            xbee.Open();
            xbee.DataReceived += new SerialDataReceivedEventHandler(xbee_DataReceived);

        }

        private void xbee_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            
            comReceive += xbee.ReadExisting().ToString();

            if (end == 0)
            {
                if (comReceive[comReceive.Length - 1] == 'E')
                {
                    end++;
                }
            }
            else if (end == 1)
            {
                if (comReceive[comReceive.Length - 1] == 'N')
                {
                    end++;
                }
            }
            else if (end == 2)
            {
                if (comReceive[comReceive.Length - 1] == 'D')
                {
                    end = 0;
                    _queue.Enqueue(comReceive);
                    _signal.Set();
                    comReceive = "";
                }
            }
        }

        private void resetTime_Click(object sender, EventArgs e)
        {
            resetTimer();
        }

        private void resetTimer()
        {
            totalTimeCSeconds = 0;
            totalTimeSeconds = 0;
            totalTimeMinutes = 0;
            totalTimeHours = 0;
            lapTimeCSeconds = 0;
            lapTimeSeconds = 0;
            lapTimeMinutes = 0;
            lapTimeHours = 0;
            lapNumbers = 0;

            lapTimes.Items.Clear();
        }

        private string formatTimeString(int hours, int minutes, int seconds, int cSeconds)
        {
            return (String.Format("{0:00}", hours) + ":" +
                 String.Format("{0:00}", minutes) + ":" +
                 String.Format("{0:00}", seconds) + ":" +
                 String.Format("{0:00}", cSeconds));
        }

        private void lapTimer_Tick_1(object sender, EventArgs e)
        {
            stopWatch.Text = formatTimeString (totalTimeHours, totalTimeMinutes,
                                               totalTimeSeconds, totalTimeCSeconds);
                
                
            if (timerOn)
            {
                totalTimeCSeconds++;
                if (totalTimeCSeconds >= 100)
                {
                    totalTimeSeconds++;
                    totalTimeCSeconds = 0;
                }
                if (totalTimeSeconds >= 60)
                {
                    totalTimeMinutes++;
                    totalTimeSeconds = 0;
                }
                if (totalTimeMinutes >= 60)
                {
                    totalTimeHours++;
                    totalTimeMinutes = 0;
                }
            }
        }

        private void lapTime_Click(object sender, EventArgs e)
        {
            string lapTimeString;
            int cSecondFix;

            if ((totalTimeCSeconds - lapTimeCSeconds) < 0)
            {
                totalTimeSeconds--;
                cSecondFix = (totalTimeCSeconds + lapTimeCSeconds) - 100;
            }
            else
            {
                cSecondFix = totalTimeCSeconds - lapTimeCSeconds;
            }

            lapNumbers++;
            lapTimeString = "Lap " + String.Format("{0:00}", lapNumbers) + ": ";
            lapTimeString += formatTimeString(totalTimeHours - lapTimeHours, totalTimeMinutes - lapTimeMinutes,
                                             totalTimeSeconds - lapTimeSeconds, cSecondFix);
            lapTimes.Items.Add(lapTimeString);

            lapTimeCSeconds = totalTimeCSeconds;
            lapTimeSeconds = totalTimeSeconds;
            lapTimeMinutes = totalTimeMinutes;
            lapTimeHours = totalTimeHours;

        }

        private void startTimer_Click(object sender, EventArgs e)
        {
            timerOn = true;
        }

        private void stopTimer_Click(object sender, EventArgs e)
        {
            timerOn = false;
        }

        private void infoRefreshTimer_Tick(object sender, EventArgs e)
        {
            string comReceive;
            while (_queue.TryDequeue(out comReceive))
            {
                comReceive = comReceive.Remove(comReceive.Length - 3);
                received.Items.Add(comReceive);
                comReceive = "";
            }
        }

        private void transmitButton_Click(object sender, EventArgs e)
        {
            string transmitData = transmit.Text + "END";
            if (xbee.IsOpen)
            {
                xbee.Write(transmitData);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
