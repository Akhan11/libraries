﻿namespace BajaIMS_GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sideInfo = new System.Windows.Forms.Panel();
            this.baudSelect = new System.Windows.Forms.Label();
            this.baudList = new System.Windows.Forms.ComboBox();
            this.comSelect = new System.Windows.Forms.Label();
            this.comList = new System.Windows.Forms.ComboBox();
            this.stopTimer = new System.Windows.Forms.Button();
            this.startTimer = new System.Windows.Forms.Button();
            this.resetTime = new System.Windows.Forms.Button();
            this.lapTime = new System.Windows.Forms.Button();
            this.stopReceive = new System.Windows.Forms.Button();
            this.startReceive = new System.Windows.Forms.Button();
            this.lapTimes = new System.Windows.Forms.ListBox();
            this.stopWatch = new System.Windows.Forms.Label();
            this.titleInfo = new System.Windows.Forms.Panel();
            this.titleTwo = new System.Windows.Forms.Label();
            this.titleOne = new System.Windows.Forms.Label();
            this.header = new System.Windows.Forms.Panel();
            this.clubLogo = new System.Windows.Forms.PictureBox();
            this.clubName = new System.Windows.Forms.Label();
            this.lapTimer = new System.Windows.Forms.Timer(this.components);
            this.received = new System.Windows.Forms.ListBox();
            this.infoRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.transmit = new System.Windows.Forms.TextBox();
            this.transmitButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sideInfo.SuspendLayout();
            this.titleInfo.SuspendLayout();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clubLogo)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // sideInfo
            // 
            this.sideInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.sideInfo.Controls.Add(this.baudSelect);
            this.sideInfo.Controls.Add(this.baudList);
            this.sideInfo.Controls.Add(this.comSelect);
            this.sideInfo.Controls.Add(this.comList);
            this.sideInfo.Controls.Add(this.stopTimer);
            this.sideInfo.Controls.Add(this.startTimer);
            this.sideInfo.Controls.Add(this.resetTime);
            this.sideInfo.Controls.Add(this.lapTime);
            this.sideInfo.Controls.Add(this.stopReceive);
            this.sideInfo.Controls.Add(this.startReceive);
            this.sideInfo.Controls.Add(this.lapTimes);
            this.sideInfo.Controls.Add(this.stopWatch);
            this.sideInfo.Controls.Add(this.titleInfo);
            this.sideInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideInfo.Location = new System.Drawing.Point(0, 0);
            this.sideInfo.Name = "sideInfo";
            this.sideInfo.Size = new System.Drawing.Size(272, 729);
            this.sideInfo.TabIndex = 0;
            // 
            // baudSelect
            // 
            this.baudSelect.AutoSize = true;
            this.baudSelect.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baudSelect.ForeColor = System.Drawing.Color.LightGray;
            this.baudSelect.Location = new System.Drawing.Point(133, 91);
            this.baudSelect.Name = "baudSelect";
            this.baudSelect.Size = new System.Drawing.Size(88, 19);
            this.baudSelect.TabIndex = 13;
            this.baudSelect.Text = "Baud Rate:";
            // 
            // baudList
            // 
            this.baudList.FormattingEnabled = true;
            this.baudList.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "115200",
            "256000"});
            this.baudList.Location = new System.Drawing.Point(136, 110);
            this.baudList.Name = "baudList";
            this.baudList.Size = new System.Drawing.Size(88, 21);
            this.baudList.TabIndex = 12;
            // 
            // comSelect
            // 
            this.comSelect.AutoSize = true;
            this.comSelect.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comSelect.ForeColor = System.Drawing.Color.LightGray;
            this.comSelect.Location = new System.Drawing.Point(13, 91);
            this.comSelect.Name = "comSelect";
            this.comSelect.Size = new System.Drawing.Size(83, 19);
            this.comSelect.TabIndex = 11;
            this.comSelect.Text = "Com Port:";
            // 
            // comList
            // 
            this.comList.FormattingEnabled = true;
            this.comList.Location = new System.Drawing.Point(16, 110);
            this.comList.Name = "comList";
            this.comList.Size = new System.Drawing.Size(88, 21);
            this.comList.TabIndex = 3;
            // 
            // stopTimer
            // 
            this.stopTimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.stopTimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stopTimer.FlatAppearance.BorderSize = 0;
            this.stopTimer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(132)))), ((int)(((byte)(159)))));
            this.stopTimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(86)))), ((int)(((byte)(105)))));
            this.stopTimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stopTimer.Font = new System.Drawing.Font("Rockwell", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopTimer.ForeColor = System.Drawing.Color.LightGray;
            this.stopTimer.Location = new System.Drawing.Point(136, 358);
            this.stopTimer.Name = "stopTimer";
            this.stopTimer.Size = new System.Drawing.Size(135, 50);
            this.stopTimer.TabIndex = 10;
            this.stopTimer.Text = "Stop";
            this.stopTimer.UseVisualStyleBackColor = false;
            this.stopTimer.Click += new System.EventHandler(this.stopTimer_Click);
            // 
            // startTimer
            // 
            this.startTimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.startTimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startTimer.FlatAppearance.BorderSize = 0;
            this.startTimer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(132)))), ((int)(((byte)(159)))));
            this.startTimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(86)))), ((int)(((byte)(105)))));
            this.startTimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startTimer.Font = new System.Drawing.Font("Rockwell", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTimer.ForeColor = System.Drawing.Color.LightGray;
            this.startTimer.Location = new System.Drawing.Point(1, 358);
            this.startTimer.Name = "startTimer";
            this.startTimer.Size = new System.Drawing.Size(135, 50);
            this.startTimer.TabIndex = 9;
            this.startTimer.Text = "Start";
            this.startTimer.UseVisualStyleBackColor = false;
            this.startTimer.Click += new System.EventHandler(this.startTimer_Click);
            // 
            // resetTime
            // 
            this.resetTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.resetTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.resetTime.FlatAppearance.BorderSize = 0;
            this.resetTime.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(132)))), ((int)(((byte)(159)))));
            this.resetTime.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(86)))), ((int)(((byte)(105)))));
            this.resetTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetTime.Font = new System.Drawing.Font("Rockwell", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetTime.ForeColor = System.Drawing.Color.LightGray;
            this.resetTime.Location = new System.Drawing.Point(137, 412);
            this.resetTime.Name = "resetTime";
            this.resetTime.Size = new System.Drawing.Size(135, 50);
            this.resetTime.TabIndex = 8;
            this.resetTime.Text = "Reset";
            this.resetTime.UseVisualStyleBackColor = false;
            this.resetTime.Click += new System.EventHandler(this.resetTime_Click);
            // 
            // lapTime
            // 
            this.lapTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.lapTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lapTime.FlatAppearance.BorderSize = 0;
            this.lapTime.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(132)))), ((int)(((byte)(159)))));
            this.lapTime.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(86)))), ((int)(((byte)(105)))));
            this.lapTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lapTime.Font = new System.Drawing.Font("Rockwell", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lapTime.ForeColor = System.Drawing.Color.LightGray;
            this.lapTime.Location = new System.Drawing.Point(2, 412);
            this.lapTime.Name = "lapTime";
            this.lapTime.Size = new System.Drawing.Size(135, 50);
            this.lapTime.TabIndex = 7;
            this.lapTime.Text = "Lap";
            this.lapTime.UseVisualStyleBackColor = false;
            this.lapTime.Click += new System.EventHandler(this.lapTime_Click);
            // 
            // stopReceive
            // 
            this.stopReceive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.stopReceive.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stopReceive.FlatAppearance.BorderSize = 0;
            this.stopReceive.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(132)))), ((int)(((byte)(159)))));
            this.stopReceive.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(86)))), ((int)(((byte)(105)))));
            this.stopReceive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stopReceive.Font = new System.Drawing.Font("Rockwell", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopReceive.ForeColor = System.Drawing.Color.LightGray;
            this.stopReceive.Location = new System.Drawing.Point(0, 207);
            this.stopReceive.Name = "stopReceive";
            this.stopReceive.Size = new System.Drawing.Size(272, 70);
            this.stopReceive.TabIndex = 6;
            this.stopReceive.Text = "Stop Receiving";
            this.stopReceive.UseVisualStyleBackColor = false;
            this.stopReceive.Click += new System.EventHandler(this.stopReceive_Click);
            // 
            // startReceive
            // 
            this.startReceive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.startReceive.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startReceive.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.startReceive.FlatAppearance.BorderSize = 0;
            this.startReceive.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(132)))), ((int)(((byte)(159)))));
            this.startReceive.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(86)))), ((int)(((byte)(105)))));
            this.startReceive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startReceive.Font = new System.Drawing.Font("Rockwell", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startReceive.ForeColor = System.Drawing.Color.LightGray;
            this.startReceive.Location = new System.Drawing.Point(0, 137);
            this.startReceive.Name = "startReceive";
            this.startReceive.Size = new System.Drawing.Size(272, 70);
            this.startReceive.TabIndex = 2;
            this.startReceive.Text = "Start Receiving";
            this.startReceive.UseVisualStyleBackColor = false;
            this.startReceive.Click += new System.EventHandler(this.startReceive_Click);
            // 
            // lapTimes
            // 
            this.lapTimes.BackColor = System.Drawing.Color.LightGray;
            this.lapTimes.Font = new System.Drawing.Font("Rockwell", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lapTimes.FormattingEnabled = true;
            this.lapTimes.ItemHeight = 23;
            this.lapTimes.Location = new System.Drawing.Point(16, 497);
            this.lapTimes.Name = "lapTimes";
            this.lapTimes.Size = new System.Drawing.Size(238, 211);
            this.lapTimes.TabIndex = 5;
            // 
            // stopWatch
            // 
            this.stopWatch.AutoSize = true;
            this.stopWatch.Cursor = System.Windows.Forms.Cursors.Default;
            this.stopWatch.Font = new System.Drawing.Font("Rockwell", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopWatch.ForeColor = System.Drawing.Color.White;
            this.stopWatch.Location = new System.Drawing.Point(15, 299);
            this.stopWatch.Name = "stopWatch";
            this.stopWatch.Size = new System.Drawing.Size(245, 49);
            this.stopWatch.TabIndex = 3;
            this.stopWatch.Text = "00:00:00:00";
            this.stopWatch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // titleInfo
            // 
            this.titleInfo.BackColor = System.Drawing.Color.DarkOrange;
            this.titleInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.titleInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.titleInfo.Controls.Add(this.titleTwo);
            this.titleInfo.Controls.Add(this.titleOne);
            this.titleInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleInfo.Font = new System.Drawing.Font("Rockwell", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleInfo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.titleInfo.Location = new System.Drawing.Point(0, 0);
            this.titleInfo.Name = "titleInfo";
            this.titleInfo.Size = new System.Drawing.Size(272, 79);
            this.titleInfo.TabIndex = 0;
            // 
            // titleTwo
            // 
            this.titleTwo.AutoSize = true;
            this.titleTwo.ForeColor = System.Drawing.Color.White;
            this.titleTwo.Location = new System.Drawing.Point(3, 39);
            this.titleTwo.Name = "titleTwo";
            this.titleTwo.Size = new System.Drawing.Size(198, 31);
            this.titleTwo.TabIndex = 1;
            this.titleTwo.Text = "ELECTRONICS";
            // 
            // titleOne
            // 
            this.titleOne.AutoSize = true;
            this.titleOne.ForeColor = System.Drawing.Color.White;
            this.titleOne.Location = new System.Drawing.Point(2, 8);
            this.titleOne.Name = "titleOne";
            this.titleOne.Size = new System.Drawing.Size(101, 31);
            this.titleOne.TabIndex = 0;
            this.titleOne.Text = "M.A.D.";
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.White;
            this.header.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.header.Controls.Add(this.clubLogo);
            this.header.Controls.Add(this.clubName);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Font = new System.Drawing.Font("Rockwell", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.header.Location = new System.Drawing.Point(272, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(736, 79);
            this.header.TabIndex = 1;
            // 
            // clubLogo
            // 
            this.clubLogo.Image = ((System.Drawing.Image)(resources.GetObject("clubLogo.Image")));
            this.clubLogo.Location = new System.Drawing.Point(6, -1);
            this.clubLogo.Name = "clubLogo";
            this.clubLogo.Size = new System.Drawing.Size(92, 76);
            this.clubLogo.TabIndex = 1;
            this.clubLogo.TabStop = false;
            // 
            // clubName
            // 
            this.clubName.AutoSize = true;
            this.clubName.ForeColor = System.Drawing.Color.Black;
            this.clubName.Location = new System.Drawing.Point(104, 21);
            this.clubName.Name = "clubName";
            this.clubName.Size = new System.Drawing.Size(531, 36);
            this.clubName.TabIndex = 0;
            this.clubName.Text = "Baja Information Monitoring System";
            // 
            // lapTimer
            // 
            this.lapTimer.Enabled = true;
            this.lapTimer.Interval = 10;
            this.lapTimer.Tick += new System.EventHandler(this.lapTimer_Tick_1);
            // 
            // received
            // 
            this.received.FormattingEnabled = true;
            this.received.Location = new System.Drawing.Point(7, 13);
            this.received.Name = "received";
            this.received.Size = new System.Drawing.Size(629, 82);
            this.received.TabIndex = 2;
            // 
            // infoRefreshTimer
            // 
            this.infoRefreshTimer.Enabled = true;
            this.infoRefreshTimer.Tick += new System.EventHandler(this.infoRefreshTimer_Tick);
            // 
            // transmit
            // 
            this.transmit.Location = new System.Drawing.Point(7, 101);
            this.transmit.Name = "transmit";
            this.transmit.Size = new System.Drawing.Size(629, 20);
            this.transmit.TabIndex = 3;
            // 
            // transmitButton
            // 
            this.transmitButton.BackColor = System.Drawing.Color.Black;
            this.transmitButton.ForeColor = System.Drawing.Color.White;
            this.transmitButton.Location = new System.Drawing.Point(642, 99);
            this.transmitButton.Name = "transmitButton";
            this.transmitButton.Size = new System.Drawing.Size(75, 23);
            this.transmitButton.TabIndex = 4;
            this.transmitButton.Text = "Send";
            this.transmitButton.UseVisualStyleBackColor = false;
            this.transmitButton.Click += new System.EventHandler(this.transmitButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(272, 79);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(736, 153);
            this.panel1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(517, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fuel Place Holder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(283, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "RPM Place Holder";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(40, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Speed Place Holder";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.transmit);
            this.panel2.Controls.Add(this.transmitButton);
            this.panel2.Controls.Add(this.received);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(272, 596);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(736, 133);
            this.panel2.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(272, 232);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(359, 364);
            this.panel3.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pitch Place Holder";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Black;
            this.label5.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(200, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Roll Place Holder";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(6, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "RD Place Holder";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Black;
            this.label7.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(200, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 19);
            this.label7.TabIndex = 6;
            this.label7.Text = "FP Place Holder";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(6, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 19);
            this.label8.TabIndex = 7;
            this.label8.Text = "FD Place Holder";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Black;
            this.label9.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(200, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 19);
            this.label9.TabIndex = 8;
            this.label9.Text = "RP Place Holder";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Black;
            this.label10.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(6, 180);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 19);
            this.label10.TabIndex = 9;
            this.label10.Text = "Gear Place Holder";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Black;
            this.label11.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(189, 180);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(149, 19);
            this.label11.TabIndex = 10;
            this.label11.Text = "Brake Place Holder";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Black;
            this.label12.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(759, 375);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 19);
            this.label12.TabIndex = 11;
            this.label12.Text = "LatLong Plot";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.header);
            this.Controls.Add(this.sideInfo);
            this.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Baja Information Monitoring System";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.sideInfo.ResumeLayout(false);
            this.sideInfo.PerformLayout();
            this.titleInfo.ResumeLayout(false);
            this.titleInfo.PerformLayout();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clubLogo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel sideInfo;
        private System.Windows.Forms.Panel header;
        private System.Windows.Forms.Panel titleInfo;
        private System.Windows.Forms.Label titleOne;
        private System.Windows.Forms.Label titleTwo;
        private System.Windows.Forms.PictureBox clubLogo;
        private System.Windows.Forms.Label clubName;
        private System.Windows.Forms.Label stopWatch;
        private System.Windows.Forms.ListBox lapTimes;
        private System.Windows.Forms.Button resetTime;
        private System.Windows.Forms.Button lapTime;
        private System.Windows.Forms.Button stopReceive;
        private System.Windows.Forms.Button startReceive;
        private System.Windows.Forms.Timer lapTimer;
        private System.Windows.Forms.Button stopTimer;
        private System.Windows.Forms.Button startTimer;
        private System.Windows.Forms.Label comSelect;
        private System.Windows.Forms.ComboBox comList;
        private System.Windows.Forms.Label baudSelect;
        private System.Windows.Forms.ComboBox baudList;
        public System.Windows.Forms.ListBox received;
        private System.Windows.Forms.Timer infoRefreshTimer;
        private System.Windows.Forms.TextBox transmit;
        private System.Windows.Forms.Button transmitButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
    }
}

