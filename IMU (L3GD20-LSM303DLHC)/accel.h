/**
Name: Arsalan Khan
SID: 200343820
File Name: accel.h
Date Started: February 12, 2018
Date Finished: February 13, 2018
*/

#include "imu.h"

// Defines so that magic numbers can be removed from code
// 0x0F is the expected value of the accel status register when a new set of
// data is avaliable
#define ACCEL_DATA_RDY           0x0F
// the following shift values were determined by checking control register 1 & 4
// on pages 24 & 26 of LSM303DLHC pdf provided
#define SAMP_FS_SHIFT 0x4
#define ON_OFF_SHIFT 0x3

/**
This represents the range of values that we can set for the accelerometers
full scale. The table for these values can be found in the LSM303DLHC pdf
on page 26. 
Below are the relevant values:

00 : +/- 2G
01 : +/- 4G
10 : +/- 8G
11 : +/- 16G
*/
typedef enum
{
	FS_2G,
	FS_4G,
	FS_8G,
	FS_16G,
} ACCEL_FS;

/**
This represents the range of values that we can set for the accelerometers
sample rate. The table for these values can be found in the LSM303DLHC pdf
on page 24.
Below are the relevant values:

0000 : PWR_DWN
0001 : 1   Hz
0010 : 10  Hz
0011 : 25  Hz
0100 : 50  Hz
0101 : 100 Hz
0110 : 200 Hz
0111 : 400 Hz
1000 : 1620 Hz
1001 : 1344 Hz (5376 Hz in low power mode)
*/
typedef enum 
{
	OFF,     // 0000 = power down
	SAMPLE_1,    // 0001 = 12.5 Hz
	SAMPLE_10,   // 0010 = 26   Hz
	SAMPLE_24,   // 0011 = 52   Hz
	SAMPLE_50,   // 0100 = 104  Hz
	SAMPLE_100,  // 0101 = 208  Hz
	SAMPLE_200,  // 0110 = 416  Hz
	SAMPLE_400,  // 0111 = 833  Hz
} ACCEL_SAMPLE_RATE;

class accel : public IMU
{
	public:
		
// Output data registers related to the accelerometer
// These values can be found in the LSM303DLHC pdf on
// page 28-29
		static const uint8_t OUT_X_L_A = 0x28;
    static const uint8_t OUT_X_H_A = 0x29;
    static const uint8_t OUT_Y_L_A = 0x2A;
    static const uint8_t OUT_Y_H_A = 0x2B;
    static const uint8_t OUT_Z_L_A = 0x2C;
    static const uint8_t OUT_Z_H_A = 0x2D;
		static const uint8_t STATUS_REG_A = 0x27;
	
// Simple constructor inits for the class
/**
* @brief  default constructor for the accel, will set pre-determined values
* @param  none
* @retval None
*/	
		accel ();

/**
* @brief  Setup for the gyro
* @note   This function will take user define accel variables and store 
          them within the class
* @param  PWR_STATE : the power state for the accel
* @param  ENABLE_AXIS : which axis you want to enable on the accel
* @param  SYSTEM_STATE : value to indicate error in system
* @param  ACCEL_FS : the fullscale value for the accel
* @param  ACCEL_SAMPLE_RATE : the data rate and bandwidth of the accel
* @retval None
*/
		accel (PWR_STATE, ENABLE_AXIS, SYSTEM_STATE, ACCEL_FS, ACCEL_SAMPLE_RATE);
	

/**
* @brief  Setup for the accelerometer
* @note   This function will take a look at the defined parameters for the class
          and then write to the relevant control registers. This will set the 
          accelerometer to be configured how we want
* @param  none
* @retval None
*/
		void accelSetup ();
/**
* @brief  Get axis values from the accelerometer if avaliable
* @note   We will check the status register for the accelerometer to asure
          that new data is avaliable before reading. Axis values will be saved
          to the class
* @param  none
* @retval None
*/
		void getAccel ();
	
	// getters and setters
/**
* @brief  Obtain a certain accelerometer register address value

* @param  reg : the register address requested
* @retval uint8_t : value of the register address requested
*/
		uint8_t getRegister(uint8_t reg);
/**
* @brief  Obtain the accelerometers set full scale value
* @param None
* @retval ACCEL_FS : value of the full scale 
*/
		ACCEL_FS getFullScale ();
/**
* @brief  Obtain the accelerometers set sample rate value
* @param None
* @retval ACCEL_SAMPLE_RATE : value of the sample rate
*/
		ACCEL_SAMPLE_RATE getSampleRate ();
/**
* @brief  set the accelerometers set full scale value
* @param ACCEL_FS : the required full scale to be set
* @retval None
*/
		void setFullScale (ACCEL_FS);
/**
* @brief  set the accelerometers set sample rate value
* @param ACCEL_SAMPLE_RATE : the required full scale to be set
* @retval None
*/
	  void setSampleRate (ACCEL_SAMPLE_RATE);

	private:
		ACCEL_FS fullScale;
		ACCEL_SAMPLE_RATE sampleRate;
		
// control registers related to the accelerometer
// These values can be found in the LSM303DLHC pdf on
// page 24 - 27
		static const uint8_t CTRL_REG1_A = 0x20;
		static const uint8_t CTRL_REG2_A = 0x21;
		static const uint8_t CTRL_REG3_A = 0x22;
		static const uint8_t CTRL_REG4_A = 0x23;
		static const uint8_t CTRL_REG5_A = 0x24;
		static const uint8_t CTRL_REG6_A = 0x25;

};

