/**
Name: Arsalan Khan
SID: 200343820
File Name: mag.cpp
Date Started: February 12, 2018
Date Finished: February 13, 2018

Purpose: The purpose of the accel.cpp/h pair is to handle all functionality related
         to the accelerometer. It will include the class type for accel, as well as
		 constructors, getters, setters, and reading functions. Below is a list of 
		 functions for this particular class. In addition to holding accelerometer
		 related functions, this class will also contain the control registers &
		 data registers associated with the device. 

///////////////////////////////////////////////////////////////////////////////
// For detailed function discriptions please refer to accel.h where the function
// is declared. Below is a name list for the utilized functions.
// This files comments will simply explain the code
///////////////////////////////////////////////////////////////////////////////

Functions: 

mag ()
mag (parameters)
magSetup
getMag
getRegister
getFullScale
getSampleRate
setFullScale
setSampleRate
*/

#include "mag.h"
#include "i2c.h"

// If no parameters are given we will simply initialize our mag
mag::mag ()
{
	onOff = PWR_ON;
	state = A_OK;
	gain = MAG_IN_8_1;
	sampleRate = SAMPLE_15_0;
}

// This constructor will take user defined values and store them within
// the class
mag::mag  (PWR_STATE pwr, SYSTEM_STATE start, MAG_GAIN fs,
               MAG_SAMPLE_RATE sample)
{
	onOff = pwr;
	state = start;
	gain = fs;
	sampleRate = sample;
}

// Control register 1 contains the registers for setting sample rate, power mode,
// and the axis configuration. They are shifted into a set variable to match the 
// register description given in the document : LSM303DLHC on page 36. Similarlly
// control register 4 has the registers for setting the full scale. This can also
// be found in the same document on page 37
void mag::magSetup ()
{
	uint8_t craSet, crbSet;
	
	
	craSet = TEMP_EN << TEMP_EN_SHIFT | sampleRate << SAMLE_RATE_SHIFT;
	crbSet = gain << GAIN_SHIFT;
	// The following do while loops are attempting to set our desired
	// registers to the indicate ctrl1Set & ctrl4Set registers. We are
	// simply writing to the required register, then reading to asure that
	// it was set. This should ideal run once, if there is an error it will repeat

	do 
	{
		i2cWrite (MAG_DEVICE_WRITE_ADDRESS, CRA_REG_M, craSet, WRITE);
		returnBuff = i2cRead (MAG_DEVICE_WRITE_ADDRESS, CRA_REG_M);
	}while (returnBuff != craSet);
	
	do 
	{
		i2cWrite (MAG_DEVICE_WRITE_ADDRESS, CRB_REG_M, crbSet, WRITE);
		returnBuff = i2cRead (MAG_DEVICE_WRITE_ADDRESS, CRB_REG_M);
	}while (returnBuff != crbSet);
	
	do
	{
		i2cWrite (MAG_DEVICE_WRITE_ADDRESS, MR_REG_M, CONT_CONVERSION, WRITE);
		returnBuff = i2cRead (MAG_DEVICE_WRITE_ADDRESS, MR_REG_M);
	}while (returnBuff != CONT_CONVERSION);
	
}

void mag::getMag ()
{
	// Check the mag staus register

	returnBuff = i2cRead (MAG_DEVICE_WRITE_ADDRESS, SR_REG_M);
	
	// If there is a new set of data ready, collect it
	if ((returnBuff & MAG_DATA_RDY) == MAG_DATA_RDY)
	{
		// each axis value is a 16 bit number, but is read as two 8 bits and
		// shifted over
		dataCollected.x = i2cRead (MAG_DEVICE_WRITE_ADDRESS, OUT_X_L_M);
		dataCollected.x = i2cRead (MAG_DEVICE_WRITE_ADDRESS, OUT_X_H_M) << HIGH_ADDR_SHIFT;

		dataCollected.y = i2cRead (MAG_DEVICE_WRITE_ADDRESS, OUT_Y_L_M);
		dataCollected.y = i2cRead (MAG_DEVICE_WRITE_ADDRESS, OUT_Y_H_M) << HIGH_ADDR_SHIFT;

		dataCollected.z = i2cRead (MAG_DEVICE_WRITE_ADDRESS, OUT_Z_L_M);
		dataCollected.z = i2cRead (MAG_DEVICE_WRITE_ADDRESS, OUT_Z_H_M) << HIGH_ADDR_SHIFT;
		
		dataCollected.temp = i2cRead (MAG_DEVICE_WRITE_ADDRESS, TEMP_OUTL);
		dataCollected.temp = i2cRead (MAG_DEVICE_WRITE_ADDRESS, TEMP_OUTH) << HIGH_ADDR_SHIFT;
	}
}

// If a non-class function requires the registers, it will return accordingly
uint8_t mag::getRegister(uint8_t reg)
{
	switch(reg)
	{
		case 1: return CRA_REG_M;
		case 2: return CRB_REG_M;
		case 3: return MR_REG_M;
		default: 
				state = ERROR_NOT_OK;
			  return 0;
	};
}
// The functions below are simply for getting and setting class values
// outside of the class.
MAG_GAIN mag::getGain()
{
	return gain;
}

MAG_SAMPLE_RATE mag::getSampleRate ()
{
	return sampleRate;
}

void mag::setGain (MAG_GAIN set)
{
	gain = set;
}
void mag::setSampleRate (MAG_SAMPLE_RATE set)
{
	sampleRate = set;
}
