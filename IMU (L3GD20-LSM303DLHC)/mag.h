/**
Name: Arsalan Khan
SID: 200343820
File Name: mag.h
Date Started: February 12, 2018
Date Finished: February 13, 2018
*/
#include "imu.h"

// Defines so that magic numbers can be removed from code
#define TEMP_EN 0x01
// the following shift values were determined by checking control register 1 & 4
// on pages 36 & 37 of LSM303DLHC pdf provided
#define TEMP_EN_SHIFT 0x7
#define SAMLE_RATE_SHIFT 0x2
#define GAIN_SHIFT 0x5
// 0x01 is the expected value of the mag status register when a new set of
// data is avaliable
#define MAG_DATA_RDY 0x01
#define CONT_CONVERSION 0x00

typedef enum
{
	SAMPLE_0_75,
	SAMPLE_1_50,
	SAMPLE_3_00,
	SAMPLE_7_50,
	SAMPLE_15_0,
	SAMPLE_30_0,
	SAMPLE_75_0,
	SAMPLE_220_0,
} MAG_SAMPLE_RATE;

typedef enum
{
	DONT_USE,
	MAG_IN_1_3,
	MAG_IN_1_9,
	MAG_IN_2_5,
	MAG_IN_4_0,
	MAG_IN_4_7,
	MAG_IN_5_6,
	MAG_IN_8_1,
} MAG_GAIN;

class mag : public IMU
{
	public:
// Output data registers related to the accelerometer
// These values can be found in the LSM303DLHC pdf on
// page 38
		static const uint8_t OUT_X_H_M = 0x03;
		static const uint8_t OUT_X_L_M = 0x04;
		static const uint8_t OUT_Z_H_M = 0x05;
		static const uint8_t OUT_Z_L_M = 0x06;
		static const uint8_t OUT_Y_H_M = 0x07;
		static const uint8_t OUT_Y_L_M = 0x08;
		static const uint8_t SR_REG_M = 0x09;
		static const uint8_t TEMP_OUTH = 0x31;
	  static const uint8_t TEMP_OUTL = 0x32;
/**
* @brief  default constructor for the mag, will set pre-determined values
* @param  none
* @retval None
*/	
		mag ();
/**
* @brief  Setup for the gyro
* @note   This function will take user define mag variables and store 
          them within the class
* @param  PWR_STATE : the power state for the mag
* @param  SYSTEM_STATE : value to indicate error in system
* @param  MAG_GAIN : the gain for the mag
* @param  MAG_SAMPLE_RATE : the sample rate of the mag
* @retval None
*/
		mag (PWR_STATE, SYSTEM_STATE, MAG_GAIN, MAG_SAMPLE_RATE);
		
/**
* @brief  Setup for the mag
* @note   This function will take a look at the defined parameters for the class
          and then write to the relevant control registers. This will set the 
          mag to be configured how we want
* @param  none
* @retval None
*/
		void magSetup ();
/**
* @brief  Get axis values from the mag if avaliable
* @note   We will check the status register for the mag to asure
          that new data is avaliable before reading. Axis values will be saved
          to the class
* @param  none
* @retval None
*/
		void getMag ();

	// getters and setters
	
/**
* @brief  Obtain a certain mag register address value

* @param  reg : the register address requested
* @retval uint8_t : value of the register address requested
*/
		uint8_t getRegister(uint8_t reg);
	
/**
* @brief  Obtain the mag set gain value
* @param  None
* @retval MAG_GAIN : value of the sample rate
*/
		MAG_GAIN getGain ();
/**
* @brief  Obtain the mag set sample rate value
* @param  None
* @retval MAG_SAMPLE_RATE : value of the sample rate
*/
		MAG_SAMPLE_RATE getSampleRate ();
/**
* @brief  set the mag set sample rate value
* @param  MAG_GAIN : the required gain to be set
* @retval None
*/
		void setGain (MAG_GAIN);
/**
* @brief  set the mag set sample rate value
* @param  MAG_SAMPLE_RATE : the required sample rate to be set
* @retval None
*/
	  void setSampleRate (MAG_SAMPLE_RATE);
	

	private:
		MAG_GAIN gain;
		MAG_SAMPLE_RATE sampleRate;
// control registers related to the mag
// These values can be found in the LSM303DLHC pdf on
// page 36 - 37
		static const uint8_t CRA_REG_M = 0x00;
		static const uint8_t CRB_REG_M = 0x01;
		static const uint8_t MR_REG_M  = 0x02;
};
