/**
Name: Arsalan Khan
SID: 200343820
File Name: accel.cpp
Date Started: February 12, 2018
Date Finished: February 13, 2018

Purpose: The purpose of the accel.cpp/h pair is to handle all functionality related
         to the accelerometer. It will include the class type for accel, as well as
		 constructors, getters, setters, and reading functions. Below is a list of 
		 functions for this particular class. In addition to holding accelerometer
		 related functions, this class will also contain the control registers &
		 data registers associated with the device. 

///////////////////////////////////////////////////////////////////////////////
// For detailed function discriptions please refer to accel.h where the function
// is declared. Below is a name list for the utilized functions.
// This files comments will simply explain the code
///////////////////////////////////////////////////////////////////////////////

Functions: 

accel ()
accel (parameters)
accelSetup
getAccel
getRegister
getFullScale
getSampleRate
setFullScale
setSampleRate

*/

#include "accel.h"
#include "i2c.h"

// If no parameters are given we will simply initialize our accelerometer
// such that it is powered on, has the ZYX axis enabled, a fullscale of 2G
// and a sample rate of 10Hz. 
accel::accel ()
{
	onOff = PWR_ON;
	state = A_OK;
	axisEnable = ENABLE_ZYX;
	fullScale = FS_2G;
	sampleRate = SAMPLE_10;
}

// This constructor will take user defined values and store them within
// the class
accel::accel  (PWR_STATE pwr, ENABLE_AXIS xyz, SYSTEM_STATE start, ACCEL_FS fs,
               ACCEL_SAMPLE_RATE sample)
{
	onOff = pwr;
	state = start;
	axisEnable = xyz;
	fullScale = fs;
	sampleRate = sample;
}

// Control register 1 contains the registers for setting sample rate, power mode,
// and the axis configuration. They are shifted into a set variable to match the 
// register description given in the document : LSM303DLHC on page 24. Similarlly
// control register 4 has the registers for setting the full scale. This can also
// be found in the same document on page 26
void accel::accelSetup ()
{
	uint8_t	ctrl1Set, ctrl4Set;
	
	ctrl1Set = sampleRate << SAMP_FS_SHIFT | onOff << ON_OFF_SHIFT | axisEnable;
	ctrl4Set = fullScale << SAMP_FS_SHIFT;

	// The following do while loops are attempting to set our desired
	// registers to the indicate ctrl1Set & ctrl4Set registers. We are
	// simply writing to the required register, then reading to asure that
	// it was set. This should ideal run once, if there is an error it will repeat
	do 
	{
		i2cWrite (ACCEL_DEVICE_WRITE_ADDRESS, CTRL_REG1_A, ctrl1Set, WRITE);
		returnBuff = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, CTRL_REG1_A);
	}while (returnBuff != ctrl1Set);
	
	do 
	{
		i2cWrite (ACCEL_DEVICE_WRITE_ADDRESS, CTRL_REG4_A, ctrl4Set, WRITE);
		returnBuff = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, CTRL_REG4_A);
	}while (returnBuff != ctrl4Set);
	
}

void accel::getAccel ()
{
	// Check the accelerometers staus register
	returnBuff = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, STATUS_REG_A);
	// If there is a new set of data ready, collect it
	if ((returnBuff & ACCEL_DATA_RDY) == ACCEL_DATA_RDY)
	{
		// each axis value is a 16 bit number, but is read as two 8 bits and
		// shifted over
		dataCollected.x = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, OUT_X_L_A);
		dataCollected.x = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, OUT_X_H_A) << HIGH_ADDR_SHIFT;

		dataCollected.y = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, OUT_Y_L_A);
		dataCollected.y = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, OUT_Y_H_A) << HIGH_ADDR_SHIFT;

		dataCollected.z = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, OUT_Z_L_A);
		dataCollected.z = i2cRead (ACCEL_DEVICE_WRITE_ADDRESS, OUT_Z_H_A) << HIGH_ADDR_SHIFT;
	}
}

// If a non-class function requires the registers, it will return accordingly
uint8_t accel::getRegister(uint8_t reg)
{
	switch(reg)
	{
		case 1: return CTRL_REG1_A;
		case 2: return CTRL_REG2_A;
		case 3: return CTRL_REG3_A;
		case 4: return CTRL_REG4_A;
		case 5: return CTRL_REG5_A;
		case 6: return CTRL_REG6_A;
		default: 
				state = ERROR_NOT_OK;
				return 0;
	};
}

// The functions below are simply for getting and setting class values
// outside of the class.
ACCEL_FS accel::getFullScale()
{
	return fullScale;
}

ACCEL_SAMPLE_RATE accel::getSampleRate ()
{
	return sampleRate;
}

void accel::setFullScale (ACCEL_FS set)
{
	fullScale = set;
}
void accel::setSampleRate (ACCEL_SAMPLE_RATE set)
{
	sampleRate = set;
}

