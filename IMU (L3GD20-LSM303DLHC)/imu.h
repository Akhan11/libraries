/**
Name: Arsalan Khan
SID: 200343820
File Name: imu.h
Date Started: February 12, 2018
Date Finished: February 13, 2018
*/

#ifndef IMU_H
#define IMU_H
#include <stdint.h>

#define HIGH_ADDR_SHIFT 0x8
// This enum is going to be used in later revisions to keep track of 
// system errors, it currently serves no purpose
typedef enum 
{
	A_OK,
	ERROR_NOT_OK,
} SYSTEM_STATE;

// Power on/off enum for the different class gyro, imu, & mag
typedef enum
{
	PWR_OFF,
	PWR_ON,
} PWR_STATE;

// the enum values for setting axis variables in the different classes
// of gyro, accel, & mag
typedef enum
{
	DISABLE_XYZ,
	ENABLE_X,
	ENABLE_Y,
	ENABLE_YX,
	ENABLE_Z,
	ENABLE_ZX,
	ENABLE_ZY,
	ENABLE_ZYX,
} ENABLE_AXIS;

// struct that is used in the base class to allow each class gyro,
// mag, & accel to have axis values & a temp value
typedef struct
{
	int16_t x, y, z, temp;
} Axis;

// Base class variables that will be inherited by the 3 core classes
class IMU
{
	public:
		
		Axis dataCollected;
		SYSTEM_STATE state;
		PWR_STATE onOff;
	    ENABLE_AXIS axisEnable;
		uint8_t returnBuff;
};

#endif
