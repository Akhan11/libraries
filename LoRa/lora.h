/** \file lora.h
\brief A general purpose library for LoRa for the use of my capstone project.

This file contains functions relevant to configuring and using LoRa module

Author: Arsalan Khan
Date Started: March 10, 2018
Date Finished: March 14, 2018

*/

#include "stdint.h"
#include "gpio.h"
#include "spi.h"
#include "commModule.h"

// LoRa Registers
#define LORA_RegFifo 0x00
#define LORA_RegOpMode 0x01
#define LORA_RegFrMsb 0x06
#define LORA_RegFrMid 0x07
#define LORA_RegFrLsb 0x08
#define LORA_RegPaConfig 0x09
#define LORA_RegFifoAddrPtr 0x0D
#define LORA_RegFifoTxBaseAd 0x0E
#define LORA_RegFifoRxBaseAd 0x0F
#define LORA_RegFifoRxCurrentAddr 0x10
#define LORA_RegIrqFlagsMask 0x11
#define LORA_RegIrqFlags 0x12
#define LORA_RegRxNbBytes 0x13
#define LORA_RegRxHeaderMSB 0x14
#define LORA_RegRxHeaderLSB 0x15
#define LORA_RegRxPacketMSB 0x16
#define LORA_RegRxPacketLSB 0x17
#define LORA_RegModemStat 0x18

#define LORA_RegHopChannel 0x1C
#define LORA_RegModemConfig1 0x1D
#define LORA_RegModemConfig2 0x1E
#define LORA_RegPreambleMsb 0x20
#define LORA_RegPreambleLsb 0x21
#define LORA_RegPayloadLength 0x22
#define LORA_RegMaxPayloadLength 0x23
#define LORA_RegFifoRxByteAddr 0x25
#define LORA_RegModemConfig3 0x26



// LoRa Commands
#define LORA_MODE_SLEEP 0x88
#define LORA_MODE_STANDBY 0x89
#define LORA_MODE_SINGLE_TRANSMIT 0x8B
#define LORA_MODE_CONT_RECIEVE 0x8D
#define LORA_MODE_SINGLE_RECIEVE 0x8E
#define LORA_MODE_CAD_MODE 0x87
#define LORA_FREQ_MSB 0xE4
#define LORA_FREQ_MID 0xE1
#define LORA_FREQ_LSB 0xC0
#define LORA_FREQ_915 0xE4E1C0

// Lora Check if CRC enabled
#define CRC_ON 0x40


// LoRa Interrupt flag status
#define PAYLOAD_CRC_ERROR 0x20
#define VALID_HEADER 0x10
#define RX_DONE 0x40 
#define TX_DONE 0x08


// general setting functions

/**
* @brief Initializes standard values for the LoRa chip as needed for our project

* @param unsigned: Indicating the pin number for CS the LoRa module is on
*/
void loraSetup(unsigned);

/**
* @brief Sets a register on the LoRa chip given data & an address
* @note Users should only be passing address values defined above
* @param addr: Address of the register
* @param data: Byte of data to be written 
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraSetReg (uint8_t addr, uint8_t data, unsigned loraPin);
/**
* @brief Checks a register on the LoRa chip given data & an address
* @note Users should only be passing address values defined above
* @param addr: Address of the register
* @param check: the data stored at the given address should match this byte
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraCheckReg (uint8_t addr, uint8_t check, unsigned loraPin);

/**
* @brief Checks if a valid message has been received & comapres it to a known value
* @note This function calls loraCheckValidReceive
* @see loraCheckValidReceive(unsigned)
* @param check*: If a data was received, comapre to known value
* @param size: Expected size of received message
* @param rxBuffer: Store the received message
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraCheckReceive (const uint8_t *check, unsigned size, uint8_t* rxBuffer, unsigned loraPin);

/**
* @brief Checks the IRQ register to see if a valid CRC was received
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraCheckCRC (unsigned loraPin);

/**
* @brief Checks the IRQ register to see if a valid RX was completed
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraValidRx (unsigned loraPin);

/**
* @brief Checks the IRQ register to see if a valid RX was completed
* @note This function calls loraCheckCRC() & loraCheckValidRx()
* @see loraCheckCrc(unsigned)
* @see loraValidRx(unsigned)
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraCheckValidReceive (unsigned loraPin);

/**
* @brief Checks the IRQ register to see if a valid Tx was completed
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraValidTx (unsigned loraPin);

/**
* @brief Loads the FiFo buffer & transmits data through the lora module
* @param buffer: Information to be transmitted
* @param size: Size of information to be transmistted
* @param loraPin: CS pin for the lora module
* @retval uint8_t: 1 or 0 depending on the truth of the check
*/
uint8_t loraSendTx (uint8_t* buffer, uint8_t size, unsigned loraPin);

/**
* @brief Set the lora module to TX mode
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraSetTx (unsigned loraPin);

/**
* @brief Set the lora module to RX mode
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraSetRx (unsigned loraPin);

/**
* @brief Set the lora module to Standby mode
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraSetStandBy (unsigned loraPin);

/**
* @brief Reset the lora module to default values
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraRst (unsigned);


// low level writing/reading functions

/**
* @brief Writes a single byte of data to the lora module at a given address
* @param addr: Address to be written to
* @param data: Data to be written
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraWriteByte (uint8_t addr, uint8_t data, unsigned loraPin);

/**
* @brief Reads a single byte of data to the lora module at a given address
* @param addr: Address to be written to
* @param data: storage space for read data
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraReadByte (uint8_t addr, uint8_t *data, unsigned loraPin);

/**
* @brief Writes a buffer of info given size, into the LoRa FiFo
* @param buffer: Buffer of information to be written
* @param size: Size of the buffer to be written
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraWriteFifo (uint8_t *buffer, uint8_t size, unsigned loraPin);

/**
* @brief Checks the FiFo of lora to determine its contents
* @note This is intended to asure the transmit message is in the FiFo buffer
        before sending. This function calls loraWriteBuffer()
* @see loraWriteBuffer (uint8_t addr, uint8_t *buffer, uint8_t size, unsigned loraPin)
* @param buffer: Buffer of information to be written
* @param size: Size of the buffer to be written
* @param compare: The buffer to be comapred to
* @param loraPin: CS pin for the lora module
* @retval 1 or 0 depending on validity of check
*/
unsigned loraCheckFifo (uint8_t *buffer, uint8_t size, uint8_t *compare, unsigned loraPin);

/**
* @brief Reads a buffer of info given size, into the LoRa FiFo
* @note This function calls loraReadBuffer ()
* @see loraReadBuffer (uint8_t addr, uint8_t *buffer, uint8_t size, unsigned loraPin)
* @param buffer: Buffer for information to be saved in
* @param size: Size of the buffer to be saved
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraReadFifo (uint8_t *buffer, uint8_t size, unsigned loraPin);

/**
* @brief Clears a portion of the FiFo buffer in module
* @param size: Size of the buffer to be cleared
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraClearFifo (uint8_t size, unsigned loraPin);

/**
* @brief Clears any given buffer for the indicated size
* @param size: Size of the buffer to be cleared
* @retval None
*/
void clearBuffer (uint8_t *buffer, uint8_t size);

/**
* @brief Writes a buffer of information using SPI
* @param addr: Address to be written to
* @param buffer: Buffer of information to be written
* @param size: Size of buffer to be written
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraWriteBuffer (uint8_t addr, uint8_t *buffer, uint8_t size, unsigned loraPin);

/**
* @brief Reads a buffer of information using SPI
* @param addr: Address to be read to
* @param buffer: Buffer for information to be stored in
* @param size: Size of buffer to be read
* @param loraPin: CS pin for the lora module
* @retval None
*/
void loraReadBuffer (uint8_t addr, uint8_t *buffer, uint8_t size, unsigned loraPin);

/**
* @brief Comapres a read array to a known constant array of information
* @note The stings must be null terminated for proper operation
* @param str1: Array we are checking the validity of
* @param str2: Constant array used for refernece
* @param size: Size of array to be compared
* @retval None
*/
uint8_t loraArrayCompare (uint8_t *str1, const uint8_t *str2, unsigned size);

/**
* @brief Calcualtes the time required for x Bytes of data to be transmitted
* @param payLoadSize: Number of bytes transmitted
* @param loraPin: CS pin for the lora module
* @retval uint8_t : time on air
*/
uint8_t loraCalculateAirTime(unsigned loraPin, uint8_t payloadSize);

/**
* @brief Returns a bandwidth value given a number between 0-9
* @retval uint8_t : bandwidth
*/
unsigned loraGetBandWidth(uint8_t bandWidthSelect);
#endif
