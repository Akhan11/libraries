#include "lora.h"
#include "lcd.h"
#include "XBee.h"
#include "sd_card.h"

//////////////////////////////////////////////////////////////
// General use LoRa functions
//////////////////////////////////////////////////////////////
void loraSetup(unsigned loraPin)
{
	uint8_t temp;
	//Put LoRa in sleep mode and configure LoRa Mode
	loraReadByte (LORA_RegOpMode, &temp, loraPin);
	if ((temp & 0x80) != 0x80)
		loraSetReg (LORA_RegOpMode, LORA_MODE_SLEEP, loraPin);

	//Setup for 915MHz frequency
	loraSetReg (LORA_RegFrMsb, LORA_FREQ_MSB, loraPin);
	loraSetReg (LORA_RegFrMid, LORA_FREQ_MID, loraPin);
	loraSetReg (LORA_RegFrLsb, LORA_FREQ_LSB, loraPin);
	
	//Enable PA Boost pin
	loraSetReg(LORA_RegPaConfig, ENABLE_PA_MODE, loraPin);
	
	// set fifo tx/rx base address
	loraSetReg (LORA_RegFifoTxBaseAd, RegFifoTxBaseAddr, loraPin);
	loraSetReg (LORA_RegFifoRxBaseAd, RegFifoRxBaseAddr, loraPin);
	
	loraSetReg (LORA_RegModemConfig1, Config1, loraPin);
	loraSetReg (LORA_RegModemConfig2, Config2, loraPin);
	loraSetReg (LORA_RegModemConfig3, Config3, loraPin);
	
	loraSetReg (LORA_RegMaxPayloadLength, MAX_PAYLOAD, loraPin);
	loraClearFifo (200, loraPin);
	
	//Put LoRa into standby mode
	loraSetReg (LORA_RegOpMode, LORA_MODE_STANDBY, loraPin);
}

// Simple function to set a given register on the LoRa chip with the indicated
// data
void loraSetReg (uint8_t addr, uint8_t data, unsigned loraPin)
{
	uint8_t buffer[1];

	// Write to the buffer, then read it to determine if the value was set.
	// repeat until write is sucessful
	do
	{
		loraWriteByte( addr, data, loraPin);
		loraReadByte (addr, buffer, loraPin);
		/**
		Implement a watchdog for this function, incase it gets stuck for later
		*/
	}while(data != buffer[0]); // This should pass on the first attempt

}

// Simple function which checks the contents of a LoRa chip register and
// compares it to a known value.
uint8_t loraCheckReg (uint8_t addr, uint8_t check, unsigned loraPin)
{
	uint8_t buffer[1];
	loraReadByte (addr, buffer, loraPin);
	if (buffer[0] == check)
		return 1;
	return 0;
}

// Function to check if a message has been received by the LoRa chip
uint8_t loraCheckReceive (const uint8_t *check, unsigned size, uint8_t* rxBuffer, unsigned loraPin)
{
	// check to see if the received message is a valid packet, or if it is garbage
	if (loraCheckValidReceive(loraPin))
	{
		// If it was valid, copy the information into a buffer
		loraReadFifo(rxBuffer, size, loraPin);
		// compare to see if it is what we expected to receive.
		if (loraArrayCompare(rxBuffer, check, size))
			return 1;
	}
	return 0;
}

// Check the interrupt flags of the LoRa chip to assure that the received message
// was valid.
uint8_t loraCheckValidReceive (unsigned loraPin)
{	
	if (loraValidRx(loraPin) && loraCheckCRC(loraPin))
	{
		// If it is a valid receive, clear the interrupt registers for future use
		loraWriteByte(LORA_RegIrqFlags, PAYLOAD_CRC_ERROR, loraPin);
		loraWriteByte(LORA_RegIrqFlags, (VALID_HEADER | RX_DONE), loraPin);
		return 1;
	}
		
	return 0;
}

// Check to see if the CRC on the received packet was valid.
uint8_t loraCheckCRC(unsigned loraPin)
{
	uint8_t buffer[1];

	// First we must asure that CRC is turned on for the LoRa module in question
	loraReadByte(LORA_RegHopChannel, buffer, loraPin);
	if ((buffer[0] & CRC_ON) == CRC_ON)
	{
		loraReadByte(LORA_RegIrqFlags, buffer, loraPin);
		if ((buffer[0] & PAYLOAD_CRC_ERROR) == 0x00)
			return 1;
	}
	return 0;
}

// Check to see if the flags for a full receive have been set on the LoRa module
uint8_t loraValidRx(unsigned loraPin)
{
	uint8_t buffer[1];

	loraReadByte(LORA_RegIrqFlags, buffer, loraPin);
	if ((buffer[0] & (VALID_HEADER | RX_DONE)) == (VALID_HEADER | RX_DONE))
		return 1;
	return 0;
}

// Check to see if the flags for a sucessful transmit have been set
uint8_t loraValidTx (unsigned loraPin)
{
	uint8_t buffer[1];
	loraReadByte (LORA_RegIrqFlags, buffer, loraPin);
	if ((buffer[0] & TX_DONE) == TX_DONE)
	{
		loraWriteByte (LORA_RegIrqFlags, TX_DONE, loraPin);
		return 1;
	}
	return 0;
}

// Given an output buffer, transmit the indicated size from the module
uint8_t loraSendTx (uint8_t* buffer, uint8_t size, unsigned loraPin)
{	
	uint8_t temp[size];
	// set the payload length of the message
	loraSetReg ( LORA_RegPayloadLength, size, loraPin);

	// write the data to the LoRa Fifo
	loraWriteFifo (buffer, size, loraPin);
	// assure that the data was written correctly
	if (loraCheckFifo (temp, size, buffer, loraPin))
	{
		// Set the module into transmit mode
		loraSetReg (LORA_RegOpMode, LORA_MODE_SINGLE_TRANSMIT, loraPin);
		// Allow the proper amount of time for on air transmission
		HAL_Delay(loraCalculateAirTime(loraPin,size)-1);
		// check if a valid transmit occured
		if (loraValidTx (loraPin))
		{
			return 1;
		}
	}
	return 0;
}

// Set the LoRa module to continuous receive mode
void loraSetRx (unsigned loraPin)
{
	loraSetReg (LORA_RegOpMode, LORA_MODE_STANDBY, loraPin);
	loraSetReg (LORA_RegOpMode, LORA_MODE_CONT_RECIEVE, loraPin);
}

// Set the LoRa module to standby mode
void loraSetStandBy (unsigned loraPin)
{
	loraSetReg (LORA_RegOpMode, LORA_MODE_STANDBY, loraPin);
}

// In the case of a chip error, this function will perform a software
// factory reset on the LoRa chip
void loraRst (unsigned loraPin)
{
	HAL_GPIO_WritePin(GPIOD, loraPin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, LORA_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay (100);
	HAL_GPIO_WritePin(GPIOD, loraPin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, LORA_EN_Pin, GPIO_PIN_SET);
	HAL_Delay (100);
}

// Write a byte of information to the LoRa chip given an address & data
void loraWriteByte (uint8_t addr, uint8_t data, unsigned loraPin)
{
	loraWriteBuffer (addr, &data, 1, loraPin);
}

// Read a byte of data from the LoRa chip given an address
void loraReadByte (uint8_t addr, uint8_t *data, unsigned loraPin)
{
	loraReadBuffer (addr, data, 1, loraPin);
}

// Write to the Lora Fifo
void loraWriteFifo (uint8_t *buffer, uint8_t size, unsigned loraPin)
{
	loraWriteByte (LORA_RegFifoAddrPtr, RegFifoTxBaseAddr, loraPin);
	loraWriteBuffer (0, buffer, size, loraPin);
}

// Check the contents of the Lora Fifo
unsigned loraCheckFifo (uint8_t *buffer, uint8_t size, uint8_t *compare, unsigned loraPin)
{
	loraWriteByte (LORA_RegFifoAddrPtr, RegFifoTxBaseAddr, loraPin);		
	loraReadBuffer (LORA_RegFifo, buffer, size, loraPin);	
	if (loraArrayCompare(buffer, compare, size))
		return 1;
	return 0;
}

// Read the Lora Fifo
void loraReadFifo (uint8_t *buffer, uint8_t size, unsigned loraPin)
{
	loraReadByte (LORA_RegFifoRxCurrentAddr, buffer, loraPin);
	loraWriteByte (LORA_RegFifoAddrPtr, buffer[0], loraPin);
	loraReadBuffer (0, buffer, size, loraPin);
}

// Clear the LoRa Fifo
void loraClearFifo (uint8_t size, unsigned loraPin)
{
	uint8_t clear[] = {0};
	loraWriteByte (LORA_RegFifoAddrPtr, RegFifoRxBaseAddr, loraPin);
	loraWriteBuffer(0, clear ,size, loraPin);
}

// null out a given buffer
void clearBuffer (uint8_t *buffer, uint8_t size)
{
	unsigned i;
	for (i = 0; i < size; i++)
		buffer[i] = 0x00;
}

// write data into a buffer provided the size using SPI communcation
void loraWriteBuffer (uint8_t addr, uint8_t *buffer, uint8_t size, unsigned loraPin)
{
	uint8_t temp;
	uint8_t i;
	addr |= 0x80;
	
	// set the CS low indicating start of transmission
	HAL_GPIO_WritePin(GPIOD, loraPin, GPIO_PIN_RESET);
	// transmist the address to be written to
	HAL_SPI_TransmitReceive(&hspi3, &addr, &temp, 1,1);
	
	// loop through all the data that needs to be sent
	for ( i = 0; i < size; i++)
	{
		HAL_SPI_Transmit(&hspi3, &buffer[i], 1, 10);
	}
	
	// set CS high indicating end of transmission
	HAL_GPIO_WritePin(GPIOD, loraPin, GPIO_PIN_SET);
}

// Read a buffer of information given a size
void loraReadBuffer (uint8_t addr, uint8_t *buffer, uint8_t size, unsigned loraPin)
{
	uint8_t temp;
	uint8_t i;
	addr &= 0x7F;
	
	// set CS low indicating start of read
	HAL_GPIO_WritePin(GPIOD, loraPin, GPIO_PIN_RESET);
	// set address to be read from
	HAL_SPI_TransmitReceive(&hspi3, &addr, &temp, 1,1);

	// Read one byte at a time for the indicated size
	for ( i = 0; i < size; i++)
	{
		HAL_SPI_Receive(&hspi3, &buffer[i], 1, 10);
	}
	
	// Set CS high indicating end of read
	HAL_GPIO_WritePin(GPIOD, loraPin, GPIO_PIN_SET);
}

// Compare two given arrays. For valid compares, it is required that these strings
// be null terminated
uint8_t loraArrayCompare (uint8_t *str1, const uint8_t *str2, unsigned size)
{
	unsigned i;
	
	for (i = 0; i < size; i++)
	{
		if (str1[i] != str2[i])
			return 0;
		if ((str1[i] == NULL_CHAR || (str2[i] == NULL_CHAR)))
			return 1;
	}
	return 0;
}

// Clauclate the amount of time required for a full message send given byte size
unsigned loraCalculateAirTime(unsigned loraPin, uint8_t payloadSize)
{
	unsigned calcA = 0;
	unsigned calcB = 0;
	
	uint64_t Rs = 0;
	uint64_t Ts = 0;
	unsigned Tpreamble = 0;
	unsigned Npreamble = 0;
	unsigned Tpayload = 0;
	unsigned Npayload = 0;
	
	uint8_t spreadFactor = 0;
	uint8_t bandWidthSelect = 0;
	unsigned bandWidth = 0;
	uint8_t codingRate = 0;
	uint8_t implicitHeader = 0;
	uint8_t lowDataRateOptimize = 0;
	uint8_t crc = 0;
	uint8_t loraInfo[2];
	
	//Get bandwidth, cording rate, implicit header, spread factor, and crc
	loraReadBuffer(LORA_RegModemConfig1, loraInfo, 2, loraPin);
	bandWidthSelect = (loraInfo[0] & 0xF0) >> 4;
	bandWidth = loraGetBandWidth(bandWidthSelect);
	codingRate = (loraInfo[0] & 0x0E) >> 1;
	implicitHeader = (loraInfo[0] & 0x01);
	spreadFactor = (loraInfo[1] & 0xF0) >> 4;
	crc = (loraInfo[1] & 0x02) >> 1;
	
	//Get low data rate optomization
	loraReadByte(LORA_RegModemConfig3,loraInfo,loraPin);
	lowDataRateOptimize = (loraInfo[0] & 0x08) >> 3;
	
	//Calculate symbol time
	Rs = (bandWidth*1000000)/power(2,spreadFactor);
	Ts = (1000000*1000000)/Rs;
	
	//Get payload length
	loraReadBuffer(LORA_RegPreambleMsb, loraInfo, 2, loraPin);
	Npreamble = (loraInfo[0] << 4) | loraInfo[1];
	
	//Calculate preamble time
	Tpreamble = (((Npreamble*100)+425)*Ts)/100;
	
	//Calculation for payload length
	calcA = ((8*payloadSize)-(4*spreadFactor)+28+(16*crc)-(20*implicitHeader));
	calcB = (4*(spreadFactor - (2*lowDataRateOptimize)));
	Npayload = 8+((calcA + calcB - 1)/calcB)*(codingRate+4);
	
	//Calculate payload time
	Tpayload = Npayload*Ts;
	
	//Return air time
	return (Tpreamble + Tpayload)/1000;
	
}

unsigned loraGetBandWidth(uint8_t bandWidthSelect)
{
	switch(bandWidthSelect)
	{
		case 0: return 7800;
		case 1: return 10400;
		case 2: return 15600;
		case 3: return 20800;
		case 4: return 31250;
		case 5: return 41700;
		case 6: return 62500;
		case 7: return 125000;
		case 8: return 250000;
		case 9: return 500000;
		default: return 0;
	}
}



